import { createContext, use, useContext, useEffect, useState } from "react";
import AppLoader from "../components/app/Loader/Loader";
import HttpService from "../services/HttpService";
import useSWR from "swr";
import { useRouter } from "next/router";
import CrudService from "/services/CrudService";
import dynamic from "next/dynamic";
import Modal from "antd/lib/modal/Modal";
import { socket } from "../socket/socket";
import { AuthContext } from "./AuthProvider";
import MainHandler from "../socket/handler/MainHandler";
export const AppContext = createContext();

// Importation dynamique
// ( le composant est chargé une fois la page chargé seulement, permet un gain de performance sur le chargement initial)
//  Utile pour les composant qui ne sont pas affiché directement au chargement de la page
const UserProfileModal = dynamic(
	() => import("../components/app/UserProfileModal/UserProfileModal"),
	{
		ssr: false,
	}
);
const ActionPopover = dynamic(
	() => import("../components/global/ActionPopover/ActionPopover"),
	{
		ssr: false,
	}
);

export default function AppProvider({ children }) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [isDomLoaded, setIsDomLoaded] = useState(false);
	const [isSocketConnected, setIsSocketConnected] = useState(false);
	const [selectedServer, setSelectedServer] = useState(null);
	const { user } = useContext(AuthContext);
	const router = useRouter();

	// Global Modal Component
	const [actionPopoverContent, setActionPopoverContent] = useState(null);
	const [userProfileModalInformations, setUserProfileModalInformations] =
		useState(false);

	// Fetch servers users
	const { data: servers, mutate: mutateServers } = useSWR(
		"/servers",
		HttpService.getData
	);

	// Fetch FriendShip relation
	const { data: friendShips, mutate: mutateFriendShips } = useSWR(
		"/friend_ship",
		HttpService.getData
	);

	/**
	 *
	 * Connexion au serveur WebSocket
	 *
	 */
	useEffect(() => {
		// Utilisateur récupérer et socket non connecté ( uniquement au chargement de la page )
		if (user && !isSocketConnected) {
			socket.connect();

			// Quand l'utilisateur vient de ce connecter
			socket.on("connect", () => {
				// Envoie un évenement au serveur websocket avec le token d'authentification de l'utilisateur
				let token = localStorage.getItem("token");
				socket.emit("connection:join", { token });
				socket.on("connection:join:response", (isOk) => {
					// le token donné par l'utilisateur n'est pas le bon
					if (!isOk) {
						HttpService.disconnectUser();
					}
					// Fin du loader, la connexion est bonne
					else {
						setIsSocketConnected(true);
						// Fonction qui enregistre tous les événement webSocket de l'application
						MainHandler(mutateFriendShips);
					}
				});
			});

			// Le socket perd la connexion
			socket.on("connect_error", (err) => {
				setIsSocketConnected(false); // Relance le loader
			});

			// Déconnexion mannuel depuis le serveur
			socket.on("disconnect", () => {
				setIsSocketConnected(false); // Relance le loader
			});
		}
	}, [user, setIsSocketConnected]);

	// OnRouteChange (Some check on selectedServer)
	useEffect(() => {
		// SelectedServer logic
		if (router.asPath.includes("@me")) setSelectedServer("@me");
		else {
			// Fast Update
			setSelectedServer({ id: router.query.server_id });
			// Verification if server exist and user as permissions
			if (router.query.server_id) {
				CrudService.get("servers", router.query.server_id).then((response) => {
					if (response.status) setSelectedServer(response.server);
					else {
						setSelectedServer("@me");
						router.push("/app/channels/@me");
					}
				});
			}
		}
	}, [router.query]);

	// Check if all data is loaded
	useEffect(() => {
		if (servers && selectedServer && friendShips) {
			setIsLoaded(true);
		}
	}, [servers, selectedServer, friendShips]);

	// Check if all images are loaded
	useEffect(() => {
		if (isLoaded) {
			let imgs = document.images,
				len = imgs.length,
				counter = 0;

			const incrementCounter = () => {
				counter++;
				if (counter === len) {
					setIsDomLoaded(true);
				}
			};

			[].forEach.call(imgs, function (img) {
				if (img.complete) incrementCounter();
				else {
					img.addEventListener("load", incrementCounter, false);
					img.addEventListener("error", incrementCounter, false);
				}
			});
		}
	}, [isLoaded]);

	return (
		<AppContext.Provider
			value={{
				servers,
				mutateServers,
				selectedServer,
				setSelectedServer,
				friendShips,
				mutateFriendShips,
				userProfileModalInformations,
				setUserProfileModalInformations,
				actionPopoverContent,
				setActionPopoverContent,
			}}
		>
			<>
				{/* Disparait une fois que le DOM est entiérement chargé ( images comprise ) */}
				<AppLoader {...{ isDomLoaded, isSocketConnected }} />

				{/* Si la page est chargé */}
				{isLoaded && (
					<>
						{/* Contenu de la page */}
						{children}

						{/**
						 *
						 *
						 * Component who can been opened on multiple page
						 *
						 */}

						{/* UserProfileModal */}
						<Modal
							open={userProfileModalInformations} // Contain {user,friendShip}  || null
							maskClosable
							destroyOnClose
							centered
							wrapClassName={"custom-modal"}
							maskStyle={{
								zIndex: "2",
								background: "rgba(0,0,0,0.85)",
							}}
							onCancel={() => {
								setUserProfileModalInformations(false);
							}}
						>
							{userProfileModalInformations && (
								<UserProfileModal
									profileUser={userProfileModalInformations.user}
									setUserProfileModalInformations={
										setUserProfileModalInformations
									}
									initialFriendShip={userProfileModalInformations.friendShip}
								/>
							)}
						</Modal>

						{/* ActionPopover */}
						<ActionPopover
							content={actionPopoverContent}
							setContent={setActionPopoverContent}
						/>
					</>
				)}
			</>
		</AppContext.Provider>
	);
}
