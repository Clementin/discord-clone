import { useRouter } from "next/router";
import React, { createContext, useEffect, useState } from "react";
import useSWR from "swr";
import HttpService from "../services/HttpService";

export const AuthContext = createContext();

export default function AuthProvider({ children }) {
	const router = useRouter();
	const [loggedIn, setLoggedIn] = useState();
	const [isLoaded, setIsLoaded] = useState(false);
	const {
		data: user,
		error,
		mutate: mutateUser,
	} = useSWR(loggedIn ? "/auth/me" : null, HttpService.postData);

	// Appelé à chaque changement de route
	useEffect(() => {
		// Si pas de token alors loggedin à faux sinon à vrai
		setLoggedIn(localStorage.getItem("token") ? true : false);
	}, [router.asPath]);

	useEffect(() => {
		if (loggedIn !== undefined) {
			manageRouting();
		}
	}, [loggedIn]);

	const manageRouting = () => {
		if (router.asPath === "/login" && loggedIn === true) {
			router.push("/app");
		}

		if (router.route.split("/")[1] == "app") {
			if (loggedIn === false) {
				router.push("/").then(() => {
					setIsLoaded(true);
				});
			} else {
				setIsLoaded(true);
			}
		} else {
			setIsLoaded(true);
		}
	};

	return isLoaded ? (
		<AuthContext.Provider value={{ loggedIn, setLoggedIn, user, mutateUser }}>
			{children}
		</AuthContext.Provider>
	) : null;
}
