import { socket } from "../socket";

export default function FriendHandler(mutateFriendShips) {
	// a la modification d'une amitié je met a jour
	socket.on("friend:friendShipStatusUpdated", () => {
		mutateFriendShips();
	});
}

export function emitFriendStatusUpdate(addressee_id) {
	let token = localStorage.getItem("token");

	socket.emit("friend:friendShipStatusUpdate", { token, addressee_id });
}
