import FriendHandler from "./FriendHandler";

export default function MainHandler(mutateFriendShips) {
	// Socket Handler for Friends Management
	FriendHandler(mutateFriendShips);
}
