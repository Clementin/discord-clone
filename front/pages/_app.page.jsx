// Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;
// Antd
import { ConfigProvider } from "antd";
import fr from "antd/lib/locale/fr_FR";
import "antd/dist/antd.css";
// Css
import "/styles/globals.less";
import "/styles/variables.less";
// Moment
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");
// Auth
import AuthProvider from "../providers/AuthProvider";
// React
import React, { useEffect, useState } from "react";
// NextJS
import { useRouter } from "next/router";
import Head from "next/head";
import AccessModal from "../components/global/AccessModal";
import HomeHeader from "../components/home/Header/Header";
import Disclaimer from "../components/global/Disclaimer/Disclaimer";
import ServerNav from "../components/app/ServerNav/ServerNav";
import AuthUser from "../components/app/AuthUser/AuthUser";
import AppProvider from "../providers/AppProvider";

function MyApp({ Component, pageProps }) {
	const router = useRouter();
	const [isApp, setIsApp] = useState();
	const [isDiscordCloneWarningOpen, setIsDiscordCloneWarningOpen] =
		useState(null);
	const disableHeader = ["/login"];

	// detect if in app or not
	useEffect(() => {
		router.route.split("/")[1] == "app" ? setIsApp(true) : setIsApp(false);
	}, [router.asPath]);

	// OnLoad
	useEffect(() => {
		setIsDiscordCloneWarningOpen(
			!window.sessionStorage.getItem("discord_clone_warning")
		);
	}, []);

	return (
		<>
			<Head>
				<title>Discord Clone | Clémentin Descamps</title>
			</Head>

			<AuthProvider>
				<ConfigProvider locale={fr}>
					<AccessModal
						{...{ isDiscordCloneWarningOpen, setIsDiscordCloneWarningOpen }}
					/>

					<Disclaimer />

					{isApp ? (
						<div id="app">
							<AppProvider>
								{/* Nav at the side who show the list of server */}
								<ServerNav />

								{/* Main component */}
								<Component {...pageProps} />

								{/* Bottom left component who show AuthUSer and audio control */}
								<AuthUser />
							</AppProvider>
						</div>
					) : (
						<div id="window">
							{/* Header de la page partie site vitrine désactivé sur la page de connexion */}
							{!disableHeader.includes(router.pathname) && <HomeHeader />}
							<Component {...pageProps} />
						</div>
					)}
				</ConfigProvider>
			</AuthProvider>
		</>
	);
}

export default MyApp;
