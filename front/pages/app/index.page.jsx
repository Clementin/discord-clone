import { useRouter } from "next/router";
import { useEffect } from "react";
import { AppIndexWrapper } from "./index.style";

export default function AppIndex() {
	const router = useRouter();

	useEffect(() => {
		router.push("/app/channels/@me");
	}, []);

	return <AppIndexWrapper></AppIndexWrapper>;
}
