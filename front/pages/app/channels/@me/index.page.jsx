import { AppContext } from "/providers/AppProvider";
import { useContext, useEffect, useState } from "react";
import PrivateMessages from "../../../../components/app/Channels/PrivateMessages/PrivateMessages";
import HeaderDivider from "../../../../components/app/HeaderDivider/HeaderDivider";
import { MyPageWrapper } from "./myPage.style";
import FriendActivity from "../../../../components/app/Channels/Me/FriendActivity/FriendActivity";
import FriendList from "../../../../components/app/Channels/Me/FriendList/FriendList";
import AddFriend from "../../../../components/app/Channels/Me/AddFriend/AddFriend";
import FriendHeader from "../../../../components/app/Channels/Me/FriendHeader/FriendHeader";

export default function MyPage() {
	const { friendShips, mutateFriendShips } = useContext(AppContext);
	const [tabOpen, setTabOpen] = useState(
		friendShips && friendShips.length ? "online" : "add"
	);
	const [filteredTabFriendShip, setFilteredTabFriendShip] = useState(null);

	// A chaque modifications de la listes des amis ou du tabOpen je filtre a nouveaux les amis
	useEffect(() => {
		if (tabOpen == "online") {
			// en ligne
			setFilteredTabFriendShip([]);
		} else {
			// Les autres cas
			setFilteredTabFriendShip(
				friendShips.filter((item) => {
					if (tabOpen == "all")
						return item.status != "blocked" && item.status != "pending"
							? true
							: false;
					else return item.status == tabOpen ? true : false;
				})
			);
		}
	}, [friendShips, tabOpen]);

	return (
		<MyPageWrapper>
			{/* Left component */}
			<PrivateMessages />

			{/* Header */}
			<FriendHeader {...{ tabOpen, setTabOpen, friendShips }} />
			<HeaderDivider />

			{/* MainContent */}
			{tabOpen == "add" ? (
				<AddFriend />
			) : (
				<FriendList {...{ tabOpen, setTabOpen, filteredTabFriendShip }} />
			)}

			{/* Right Component */}
			<FriendActivity />
		</MyPageWrapper>
	);
}
