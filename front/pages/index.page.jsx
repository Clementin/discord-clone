import { IndexWrapper } from "./index.style";
import Image from "next/image";
import Link from "next/link";
import { useContext } from "react";
import { Button } from "antd";
import { AuthContext } from "/providers/AuthProvider";

export default function Home() {
	const { loggedIn } = useContext(AuthContext);

	return (
		<IndexWrapper>
			<div id="top-section">
				<div className="text-content-container">
					<h1>Imagine a place...</h1>
					<p>
						...where you can belong to a school club, a gaming group, or a
						worldwide art community. Where just you and a handful of friends can
						spend time together. A place that makes it easy to talk every day
						and hang out more often.
					</p>
					<a href={loggedIn ? "/app/channels/@me" : "/login"}>
						<button>Open Discord in your browser</button>
					</a>
				</div>

				{/* Décoration for top-section */}
				<img
					id="background-img"
					src="/assets/img/index/background_img.svg"
					alt="background"
				/>

				<img
					id="illu-1"
					src="/assets/img/index/illu-1.svg"
					alt="illustration"
				/>
				<img
					id="illu-2"
					src="/assets/img/index/illu-2.svg"
					alt="illustration"
				/>
			</div>

			<section className="section-1">
				<div className="section-container">
					<img src="/assets/img/index/illu-3.svg" alt="illustration" />
					<div className="section-content-text">
						<h3>Create an invite-only place where you belong</h3>
						<p>
							Discord servers are organized into topic-based channels where you
							can collaborate, share, and just talk about your day without
							clogging up a group chat.
						</p>
					</div>
				</div>
			</section>

			<section className="section-2">
				<div className="section-container">
					<img src="/assets/img/index/illu-4.svg" alt="illustration" />
					<div className="section-content-text">
						<h3>Where hanging out is easy</h3>
						<p>
							Grab a seat in a voice channel when you’re free. Friends in your
							server can see you’re around and instantly pop in to talk without
							having to call.
						</p>
					</div>
				</div>
			</section>

			<section className="section-3">
				<div className="section-container">
					<img src="/assets/img/index/illu-5.svg" alt="illustration" />
					<div className="section-content-text">
						<h3>From few to a fandom</h3>
						<p>
							Get any community running with moderation tools and custom member
							access. Give members special powers, set up private channels, and
							more.
						</p>
					</div>
				</div>
			</section>

			<section className="section-4">
				<div className="section-container">
					<h3>reliable tech for staying close</h3>
					<p>
						Low-latency voice and video feels like you’re in the same room. Wave
						hello over video, watch friends stream their games, or gather up and
						have a drawing session with screen share.
					</p>
					<img
						className="illu-6"
						src="/assets/img/index/illu-6.svg"
						alt="illustration"
					/>

					<div className="h4-container">
						<img src="/assets/img/index/illu-7.svg" alt="stars" />
						<h4>Ready to start your journey?</h4>
					</div>

					<a
						href={loggedIn ? "/app/channels/@me" : "/login"}
						className="login-link"
					>
						<Button type="primary">Open Discord in your browser</Button>
					</a>
				</div>
			</section>
		</IndexWrapper>
	);
}
