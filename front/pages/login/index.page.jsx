import { useEffect, useState } from "react";
import LoginForm from "../../components/login/LoginForm/LoginForm";
import RegisterForm from "../../components/login/RegisterForm/RegisterForm";
import { LoginWrapper } from "./login.style";

export default function Login() {
	//  0 == login / 1 == register
	const [formState, setFormState] = useState(0);
	const [activeForm, setActiveForm] = useState(null);

	const handleChangeState = () => {
		activeForm.className =
			"ant-form ant-form-vertical auth-form-container form form-disapear";

		setTimeout(() => {
			setFormState(!formState);
		}, 300);
	};

	// Change ActiveForm
	useEffect(() => {
		setActiveForm(document.getElementsByClassName("form")[0]);
	}, [formState]);

	// Make activeForm appear
	useEffect(() => {
		if (activeForm)
			activeForm.className =
				"ant-form ant-form-vertical auth-form-container form form-appear";
	}, [activeForm]);

	return (
		<LoginWrapper>
			{!formState ? (
				<LoginForm {...{ handleChangeState }} />
			) : (
				<RegisterForm {...{ handleChangeState }} />
			)}
		</LoginWrapper>
	);
}
