import styled from "styled-components";

export const LoginWrapper = styled.div`
	background: url("/assets/img/login/background.svg");
	height: 100vh;
	width: 100vw;
	background-size: cover;
	position: relative;
	top: 0px;
	left: 0px;
	display: flex;
	flex-direction: column;

	.auth-form-container {
		opacity: 0;
		position: relative;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: max-content;
		background-color: var(--primary-dark-600);
		padding: 32px;
		border-radius: 5px;
		-webkit-box-shadow: var(--dark-elevation-high);
		box-shadow: var(--dark-elevation-high);
		transition: 0.3s;

		.form-text-content {
			max-width: 414px;
			h2 {
				color: white;
				text-align: center;
				font-size: 24px;
				font-weight: 600;
				margin-bottom: 5px;
			}

			p {
				text-align: center;
				color: #b9bbbe;
				font-size: 16px;
			}
		}

		.ant-form-item-label {
			margin-bottom: 8px;
			padding: 0px;

			label {
				color: #b9bbbe;
				text-transform: uppercase;
				font-weight: bold;
				font-size: 12px;
				position: relative;

				&::before {
					position: absolute;
					right: -12px;
					top: 0px;
					font-size: 9px;
				}
			}
		}

		.validation-fail {
			label {
				color: #f38688;

				span {
					font-size: 10px;
					font-style: italic;
					font-weight: 400;
					text-transform: none;
				}

				&::before {
					display: none;
				}
			}
		}

		input {
			width: 414px;
			height: 40px;
			background-color: #202225 !important;
			border: none;
			color: var(--text-normal);
			font-size: 16px;
		}

		textarea {
			resize: none;
			overflow: hidden;
			width: 414px;
			background-color: #202225 !important;
			border: none;
			color: var(--text-normal);
			font-size: 16px;
		}

		.textarea-item {
			position: relative;

			&::after {
				content: "Max char : 200";
				position: absolute;
				width: max-content;
				color: #b9bbbe;
				bottom: -20px;
				font-size: 10px;
				color: white;
				left: 0px;
				z-index: 1;
			}
		}

		.ant-btn-primary {
			width: 100%;
			font-weight: 600;
			font-size: 16px;
			height: 44px;
			margin-top: 10px;
		}

		.change-form-state {
			color: #a3a6aa;
			margin-top: 5px;

			button {
				color: #00aff4;
				outline: none;
				cursor: pointer;
				background: transparent;
				border: none;
				font-family: "gg sans";
				margin: 0px;
				padding: 0px;

				&:hover {
					text-decoration: underline;
				}
			}
		}
	}

	.form-disapear {
		top: 30%;
		opacity: 0;
	}

	.form-appear {
		top: 50%;
		opacity: 1;
	}

	.ant-spin {
		i {
			background-color: white;
		}
	}
`;
