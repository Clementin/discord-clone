import styled from "styled-components";

export const IndexWrapper = styled.div`
	#top-section {
		background: #404eed;
		min-height: 626px;
		position: relative;
		align-items: center;
		height: 626px;

		.text-content-container {
			max-width: 780px;
			padding: 0px var(--page-gutter);
			margin: 0px auto;
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			height: 100%;
			z-index: 1;
			position: relative;

			h1 {
				text-transform: uppercase;
				color: white;
				font-family: "kanit";
				font-weight: bold;
				font-size: 75px;
				margin-bottom: 10px;
			}

			p {
				color: white;
				text-align: center;
				font-size: 20px;
			}

			button {
				border-radius: 28px;
				font-size: 18px;
				padding: 16px 32px;
				background-color: var(--not-quite-black);
				color: white;
				border: none;
				outline: none;
				cursor: pointer;
				margin-top: 20px;
				transition: 0.3s;

				&:hover {
					background-color: hsl(
						220,
						calc(var(--saturation-factor, 1) * 7.7%),
						22.9%
					);
					-webkit-box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
					box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
				}
			}
		}

		#background-img {
			top: 0px;
			left: 50%;
			margin-left: -1280px;
			position: absolute;
		}

		#illu-1 {
			position: absolute;
			bottom: 0px;
			left: 50%;
			margin-left: -1030px;
		}
		#illu-2 {
			position: absolute;
			bottom: 0px;
			left: 50%;
			margin-left: 370px;
		}
	}

	.section-1,
	.section-3 {
		flex-direction: row;
	}

	.section-2 {
		background: #f6f6f6;

		.section-container {
			flex-direction: row-reverse;
		}
	}

	section {
		display: flex;
		background: white;
		align-items: center;
		justify-content: center;
		width: 100%;

		.section-container {
			padding: 120px 40px;
			max-width: 1260px;
			width: 100%;
			display: flex;
			align-items: center;
			justify-content: space-between;

			.section-content-text {
				max-width: 380px;
				width: max-content;

				h3 {
					font-weight: bold;
					font-size: 46px;
					line-height: 1.2;
				}

				p {
					color: #23272a;
					font-size: 18px;
					font-weight: 400;
				}
			}
		}
	}

	.section-4 {
		flex-direction: column;
		background: #f6f6f6;

		.section-container {
			flex-direction: column;

			h3 {
				font-weight: bold;
				font-family: "kanit";
				font-size: 54px;
				text-transform: uppercase;
				margin-bottom: 10px;
				text-align: center;
			}

			p {
				color: #23272a;
				font-size: 22px;
				font-weight: 400;
				text-align: center;
			}

			.illu-6 {
				width: 100%;
			}

			.h4-container {
				margin-top: 100px;
				position: relative;

				img {
					top: -30px;
					position: absolute;
					left: 50%;
					transform: translateX(-50%);
				}

				h4 {
					text-align: center;
					font-weight: bold;
					font-size: 32px;
					margin-bottom: 40px;
				}
			}

			button {
				padding: 16px 32px;
				border-radius: 40px;
				font-size: 18px;
				height: max-content;
				transition: 0.2s;

				&:hover {
					background-color: hsl(
						235,
						calc(var(--saturation-factor, 1) * 86.1%),
						71.8%
					);
					-webkit-box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
					box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
				}
			}
		}
	}
`;
