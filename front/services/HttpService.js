export default class HttpService {
  static putData = async (url, data) => {
    return fetch(
      process.env.NEXT_PUBLIC_API_URL + url,
      this.putRequestOptions(data)
    ).then((response) => {
      if (response.status && response.status === 401)
        return this.disconnectUser();
      else {
        return response.json();
      }
    });
  };

  static postData = async (url, data) => {
    return fetch(
      process.env.NEXT_PUBLIC_API_URL + url,
      this.postRequestOptions(data)
    ).then((response) => {
      if (response.status && response.status === 401)
        return this.disconnectUser();
      else {
        return response.json();
      }
    });
  };

  static getData = async (url) => {
    return fetch(
      process.env.NEXT_PUBLIC_API_URL + url,
      this.getRequestOptions()
    ).then((response) => {
      if (response.status && response.status === 401)
        return this.disconnectUser();
      else {
        return response.json();
      }
    });
  };

  static deleteData = async (url) => {
    return fetch(
      process.env.NEXT_PUBLIC_API_URL + url,
      this.deleteRequestOptions()
    ).then((response) => {
      if (response.status && response.status === 401)
        return this.disconnectUser();
      else {
        return response.json();
      }
    });
  };

  static postRequestOptions(item) {
    let requestOptions = {
      method: "POST",
      headers: this.getHeaders(),
      body: JSON.stringify(item),
    };
    return requestOptions;
  }

  static putRequestOptions(data) {
    let requestOptions = {
      method: "PUT",
      headers: this.getHeaders(),
      body: JSON.stringify(data),
    };
    return requestOptions;
  }

  static getRequestOptions() {
    let requestOptions = {
      method: "GET",
      headers: this.getHeaders(),
    };
    return requestOptions;
  }

  static deleteRequestOptions() {
    let requestOptions = {
      method: "DELETE",
      headers: this.getHeaders(),
    };
    return requestOptions;
  }

  static disconnectUser() {
    localStorage.clear();
    sessionStorage.clear();
    location.reload();
    return "error";
  }

  static getHeaders() {
    return {
      Authorization: "Bearer " + localStorage.getItem("token"),
      Accept: "application/json",
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Request-Method": "GET, POST, PUT, OPTIONS",
    };
  }

  postFileData = async (item, url) => {
    const requestOptions = this.postFileRequestOptions(item);
    return fetch(process.env.NEXT_PUBLIC_API_URL + url, requestOptions).then(
      (response) => response
    );
  };

  postFileRequestOptions = (item) => {
    let requestOptions = {
      method: "POST",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Request-Method": "GET, POST, PUT, OPTIONS",
      },
      body: item,
    };

    return requestOptions;
  };
}
