import HttpService from "./HttpService";

export default class CrudService {
  static create(entity, data) {
    return new Promise((resolve, reject) => {
      HttpService.postData(`/${entity}`, data)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  }

  static getAll(entity) {
    return new Promise((resolve, reject) => {
      HttpService.getData(`/${entity}`)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  }

  static get(entity, key) {
    return new Promise((resolve, reject) => {
      HttpService.getData(`/${entity}/` + key)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  }

  static update(entity, data, key) {
    return new Promise((resolve, reject) => {
      HttpService.putData(`/${entity}/` + key, data)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  }

  static delete(entity, key) {
    return new Promise((resolve, reject) => {
      HttpService.deleteData(`/${entity}/` + key)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
  }
}
