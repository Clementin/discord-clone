import HttpService from "./HttpService";

export default class AuthService {
	// Tentative de connexion d'un utilisateur
	static connect(email, password) {
		return new Promise((resolve, reject) => {
			HttpService.postData("/auth", { email: email, password: password }).then(
				(response) => {
					// 	if (resp && resp.token) {
					// 		localStorage.setItem("token", resp.token);
					// 		resolve(resp.user);
					// 	} else {
					// 		resolve("error");
					// 	}
					resolve(response);
				}
			);
		});
	}
}
