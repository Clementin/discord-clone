import { Button, Form, Input, Spin } from "antd";
import { useContext, useEffect, useState } from "react";
import AuthService from "../../../services/AuthService";
import { LoginFormWrapper } from "./LoginForm.style";
import { requiredRules } from "/helpers/form.js";
import { AuthContext } from "/providers/AuthProvider";
import { useRouter } from "next/router";

export default function LoginForm({ handleChangeState }) {
	const [form] = Form.useForm();
	const { loggedIn, setLoggedIn } = useContext(AuthContext);
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [responseError, setResponseError] = useState(null);

	useEffect(() => {
		if (loggedIn) router.push("/app");
	}, [loggedIn]);

	const handleSubmit = () => {
		setLoading(true);
		AuthService.connect(
			form.getFieldValue("email"),
			form.getFieldValue("password")
		).then((response) => {
			if (response.status) {
				setResponseError(null);

				localStorage.setItem("token", response.token);
				setLoggedIn(true);
			} else {
				setResponseError(response);
			}

			setLoading(false);
		});
	};

	return (
		<LoginFormWrapper>
			<Form form={form} className="auth-form-container form" layout="vertical">
				<div className="form-text-content">
					<h2>Ha, te revoilà !</h2>
					<p>Nous somme si heureux de te revoir !</p>
				</div>

				<Form.Item
					className={responseError?.errors?.email && "validation-fail"}
					name="email"
					label={
						<label>
							e-mail{" "}
							{
								<span>
									{" "}
									{responseError?.errors?.email &&
										" - " + responseError.errors.email[0]}
								</span>
							}{" "}
						</label>
					}
				>
					<Input type="text" />
				</Form.Item>

				<Form.Item
					className={responseError?.errors?.password && "validation-fail"}
					name="password"
					label={
						<label>
							mot de passe{" "}
							{
								<span>
									{" "}
									{responseError?.errors?.password &&
										" - " + responseError.errors.password[0]}
								</span>
							}{" "}
						</label>
					}
				>
					<Input type="password" />
				</Form.Item>

				<Button type="primary" htmlType="submit" onClick={handleSubmit}>
					{loading ? <Spin /> : "Connexion"}
				</Button>
				<p className="change-form-state">
					Besoin d'un compte ?{" "}
					<button onClick={handleChangeState}> Demander un accès</button>
				</p>
			</Form>
		</LoginFormWrapper>
	);
}
