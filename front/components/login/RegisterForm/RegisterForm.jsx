import { Button, Form, Input } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { RegisterFromWrapper } from "./RegisterForm.style";
import { requiredRules } from "/helpers/form.js";

export default function RegisterForm({ handleChangeState }) {
	const [form] = Form.useForm();

	return (
		<RegisterFromWrapper>
			<Form form={form} className="auth-form-container form" layout="vertical">
				<div className="form-text-content">
					<h2>Demande d'accès</h2>
					<p>
						Ce site étant un clone, merci d'expliquer pourquoi vous voulez
						accéder au site sans que je vous y invite moi-même
					</p>
				</div>

				<Form.Item label="e-mail" rules={[requiredRules]}>
					<Input type="text" />
				</Form.Item>

				<Form.Item label="nom d'utilisateur" rules={[requiredRules]}>
					<Input type="text" />
				</Form.Item>

				<Form.Item
					label="message"
					rules={[requiredRules]}
					className="textarea-item"
				>
					<TextArea rows={3} type="text" maxLength={200} />
				</Form.Item>

				<Button type="primary" htmlType="submit">
					Demander mon accès ( Désactivé )
				</Button>
				<p className="change-form-state">
					<button onClick={handleChangeState}>Tu as déjà un compte ?</button>
				</p>
			</Form>
		</RegisterFromWrapper>
	);
}
