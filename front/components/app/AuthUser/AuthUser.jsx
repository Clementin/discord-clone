import { useContext, useEffect } from "react";
import { AuthUserWrapper } from "./AuthUser.style";
import { AuthContext } from "/providers/AuthProvider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faGear,
	faHeadphones,
	faMicrophone,
} from "@fortawesome/free-solid-svg-icons";
import { Tooltip } from "antd";

export default function AuthUser() {
	const { user } = useContext(AuthContext);

	return (
		<AuthUserWrapper>
			<div className="user-info">
				<div className="profile-pic-container">
					{user.picture ? (
						<img
							className="user-pic"
							src="https://cdn.discordapp.com/avatars/132076642133737472/23b60c648a1417e0fb8fe3664d883044.webp?size=32"
							alt="user-pic"
						/>
					) : (
						<div
							className="user-pic-default"
							style={{ backgroundColor: user.color }}
						>
							<img src="/assets/logo/logo.png" alt="logo" />
						</div>
					)}
				</div>

				<div className="nickname-container">
					<div className="nickname">{user.nickname}</div>
					<p className="id">#1111</p>
				</div>
			</div>

			<div className="user-control">
				<Tooltip
					title="Mettre en sourdine"
					placement="top"
					overlayClassName="server-popover"
					color="#18191c"
				>
					<div className="item-container disabled">
						<FontAwesomeIcon className="item" icon={faMicrophone} />
					</div>
				</Tooltip>

				<Tooltip
					title="Rendre muet"
					placement="top"
					overlayClassName="server-popover"
					color="#18191c"
				>
					<div className="item-container">
						<FontAwesomeIcon className="item" icon={faHeadphones} />
					</div>
				</Tooltip>

				<Tooltip
					title="Paramètre utilisateur"
					placement="top"
					overlayClassName="server-popover"
					color="#18191c"
				>
					<div className="item-container">
						<FontAwesomeIcon className="item" icon={faGear} />
					</div>
				</Tooltip>
			</div>
		</AuthUserWrapper>
	);
}
