import styled from "styled-components";

export const AuthUserWrapper = styled.div`
	width: 240px;
	min-width: 240px;
	max-width: 240px;
	height: 52px;
	position: absolute;
	bottom: 0px;
	left: 72px;
	background: #292b2f;
	padding: 0px 8px;
	display: flex;
	flex-direction: row;
	align-items: center;

	.user-info {
		display: flex;
		align-items: center;
		cursor: pointer;
		border-radius: 4px;
		padding: 4px 5px 5px 2px;
		max-width: 122px;
		width: 122px;
		min-width: 122px;

		&:hover {
			background: rgba(79, 84, 92, 0.6);
		}

		.profile-pic-container {
			display: flex;
			height: 32px;
			width: 32px;
			margin-right: 7px;

			.user-pic {
				border-radius: 50%;
				object-fit: cover;
			}

			.user-pic-default {
				border-radius: 50%;
				width: 32px;
				position: relative;
				max-width: 32px;

				img {
					position: absolute;
					top: 47%;
					left: 50%;
					transform: translate(-50%, -50%);

					/* object-fit: cover; */
					width: 100%;
					padding: 6px;
				}
			}
		}

		.nickname-container {
			display: flex;
			flex-direction: column;

			p {
				margin: 0px;
				line-height: 1;
			}

			.nickname {
				color: white;
				font-weight: 600;
				width: 76px;
				max-width: 76px;
				text-overflow: ellipsis;
				white-space: nowrap;
				overflow: hidden;

				margin: 0px;
			}

			.id {
				color: #b9bbbe;
				font-size: 12px;
			}
		}
	}

	.user-control {
		height: 32px;
		margin-left: auto;
		display: flex;
		align-items: center;

		.item-container {
			width: 32px;
			max-width: 32px;
			height: 32px;
			position: relative;

			.item {
				width: 32px;
				max-width: 32px;
				height: 32px;
				background: transparent;
				border-radius: 5px;
				padding: 8px;
				color: #b9bbbe;
				cursor: pointer;
				box-sizing: border-box;

				&:hover {
					background: rgba(79, 84, 92, 0.6);
				}
			}

			&.disabled {
				&::before {
					content: "";
					position: absolute;
					width: 20px;
					height: 2px;
					background: #ed4245;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%) rotate(135deg);
				}
			}
		}
	}
`;
