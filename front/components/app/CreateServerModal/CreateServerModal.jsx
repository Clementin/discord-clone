import {
	faAngleRight,
	faClose,
	faPlus,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Form, Input, Modal, Spin, Upload } from "antd";
import { useEffect, useState } from "react";
import { CreateServerModalWrapper } from "./CreateServerModal.style";
import { getBase64 } from "../../../helpers/photo";
import CrudService from "/services/CrudService";
import { useRouter } from "next/router";

export default function CreateServerModal({
	isCreateServerOpen,
	setIsCreateServerOpen,
	mutateServers,
}) {
	const [step, setStep] = useState("create_scratch_1");
	const [serverPicture, setServerPicture] = useState(null);
	const [form] = Form.useForm();
	const [loading, setLoading] = useState(false);
	const nameWatcher = Form.useWatch("name", form);
	const router = useRouter();

	// Reset onClose
	useEffect(() => {
		if (!isCreateServerOpen) {
			setStep(null);
			form.resetFields();
			setServerPicture(null);
		} else setStep("create_scratch_1");
	}, [isCreateServerOpen]);

	// OnStepChange
	useEffect(() => {
		let stepContainers = document.getElementsByClassName("step-container");

		for (let container of stepContainers) {
			container.style.zIndex = container.id == step ? 1 : -1;
		}
	}, [step]);

	const handleCreateServer = (data) => {
		if (loading) return; // If loading dont trigger other request

		if (serverPicture) {
			data.picture = serverPicture;
		}

		if (data.name) {
			CrudService.create("servers", data).then((response) => {
				setLoading(true);
				if (response.status) {
					form.resetFields();
					setServerPicture(null);
					mutateServers();
					setLoading(false);
					setIsCreateServerOpen(false);
					router.push("/app/channels/" + response.server.id + "/channel");
				}
			});
		}
	};

	const handleUploadServerPictureChange = async (file, fileList, event) => {
		if (file.file.status == "done") {
			let base64 = await getBase64(file.file.originFileObj);
			setServerPicture(base64);
		}
	};

	return (
		<CreateServerModalWrapper>
			<Modal
				className="create-server-modal"
				closable={false}
				open={isCreateServerOpen}
				centered
				destroyOnClose
				maskClosable
				onCancel={() => {
					setIsCreateServerOpen(false);
				}}
				maskStyle={{
					background: "rgba(0,0,0,0.85)",
				}}
			>
				<div
					id="create_scratch_1"
					className={
						"step-container" +
						(step == "create_scratch_1" ? " selected-state" : "")
					}
				>
					<FontAwesomeIcon
						icon={faClose}
						onClick={() => {
							setIsCreateServerOpen(false);
						}}
					/>

					<div className="body">
						<h2>Créer un serveur</h2>
						<p className="description">
							Ton serveur est l'endroit ou tu te retrouves tes amis. Crée le
							tien et lance une discussion.
						</p>

						<div id="buttons-container">
							<button
								onClick={() => {
									setStep("create_scratch_2");
								}}
							>
								<img src="/assets/img/createForm/illu-1.svg" />
								<span>Créer le mien</span>
								<FontAwesomeIcon icon={faAngleRight} />
							</button>

							<h3>Commencer à partir d'un modèle</h3>

							<button>
								<img src="/assets/img/createForm/illu-2.svg" />
								<span>Bientôt...</span>
								<FontAwesomeIcon icon={faAngleRight} />
							</button>

							<button>
								<img src="/assets/img/createForm/illu-3.svg" />
								<span>Bientôt...</span>
								<FontAwesomeIcon icon={faAngleRight} />
							</button>

							<button>
								<img src="/assets/img/createForm/illu-4.svg" />
								<span>Bientôt...</span>
								<FontAwesomeIcon icon={faAngleRight} />
							</button>
						</div>
					</div>

					<div className="footer">
						<h3>Tu as déjà une invitation ?</h3>
						<Button type="primary">Rejoindre un serveur</Button>
					</div>
				</div>

				<div
					id="create_scratch_2"
					className={
						"step-container" +
						(step == "create_scratch_2" ? " selected-state" : "")
					}
				>
					<Form
						form={form}
						className="form"
						layout="vertical"
						onFinish={handleCreateServer}
					>
						<FontAwesomeIcon
							icon={faClose}
							onClick={() => {
								setIsCreateServerOpen(false);
							}}
						/>

						<div className="body">
							<h2>Personnalise ton serveur</h2>
							<p className="description">
								Donne une personnalité à ton nouveau serveur en choisissant un
								nom et une icône. Tu pourras toujours les modifiers plus tard.
							</p>

							<Form.Item name={"picture"} className="upload-server-picture">
								<Upload
									accept=".jpg, .jpeg, .png, .svg"
									showUploadList={false}
									onChange={handleUploadServerPictureChange}
								>
									{serverPicture ? (
										<img
											className="server-picture"
											src={serverPicture}
											alt="uploaded server picture"
										/>
									) : (
										<img src="/assets/img/createForm/upload.png" alt="upload" />
									)}
								</Upload>
							</Form.Item>

							<Form.Item name="name" label="nom du serveur">
								<Input />
							</Form.Item>
						</div>

						<div className="footer-finish">
							<button
								className="back-btn"
								onClick={() => {
									setStep("create_scratch_1");
								}}
							>
								Retour
							</button>
							<Button
								className="submit-btn"
								type="primary"
								htmlType="submit"
								disabled={!nameWatcher || !nameWatcher.length}
							>
								{loading ? <Spin /> : "Créer"}
							</Button>
						</div>
					</Form>
				</div>
			</Modal>
		</CreateServerModalWrapper>
	);
}
