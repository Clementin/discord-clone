import styled from "styled-components";

export const ServerNavWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	position: relative;
	left: 0px;
	height: 100%;
	background: #202225;
	width: 72px;
	min-width: 72px;
	max-width: 72px;
	padding: 12px 0px;

	.server-container {
		position: relative;
	}

	.server {
		margin-bottom: 8px;
		border-radius: 50%;
		display: flex;
		align-items: center;
		justify-content: center;
		height: 48px;
		width: 48px;
		background-color: hsl(220, calc(var(--saturation-factor, 1) * 7.7%), 22.9%);
		transition: 0.1s;

		cursor: pointer;
		overflow: clip;

		&:active {
			top: -1px;
		}

		&:hover {
			border-radius: 16px;

			&::before {
				content: "";
				position: absolute;
				height: 20px;
				width: 8px;
				left: -17px;
				top: 50%;
				transform: translateY(-50%);
				background: white;
				border-radius: 0px 4px 4px 0px;
			}
		}

		&::before {
			transition: 0.2s;
		}

		.server-logo {
			object-fit: cover;
			width: 100%;
			height: 100%;
		}

		.server-no-logo {
			color: #dcddde;
			margin: 0px;
			font-weight: 600;
		}

		&.private-message {
			.server-logo {
				padding: 10px;
				object-fit: contain;
			}
		}

		&.server-selected {
			border-radius: 16px;
			background-color: hsl(235, calc(1 * 85.6%), 64.7%);

			&::before {
				content: "";
				position: absolute;
				height: 40px;
				width: 8px;
				left: -17px;
				top: 50%;
				transform: translateY(-50%);
				background: white;
				border-radius: 0px 4px 4px 0px;
			}

			&:hover {
				background-color: hsl(235, calc(1 * 85.6%), 64.7%);

				&::before {
					content: "";
					position: absolute;
					height: 40px;
					width: 8px;
					left: -17px;
					top: 50%;
					transform: translateY(-50%);
					background: white;
					border-radius: 0px 4px 4px 0px;
				}
			}
		}

		&.add-server {
			display: flex;
			align-items: center;
			justify-content: center;

			.fa-plus {
				color: #3ba55d;
				font-size: 18px;
			}

			&:hover {
				background: hsl(139, calc(var(--saturation-factor, 1) * 47.3%), 43.9%);

				.fa-plus {
					color: white;
				}
			}
		}
	}

	.serverNav-divider {
		width: 32px;
		height: 2px;
		margin-bottom: 8px;
		background-color: rgba(79, 84, 92, 0.48);
	}

	.ant-badge {
		.ant-badge-count {
			border: 4px solid #202225;
		}
	}
`;
