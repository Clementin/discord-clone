import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import { ServerNavWrapper } from "./ServerNav.style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { Badge, Tooltip } from "antd";
import { AppContext } from "../../../providers/AppProvider";
import CreateServerModal from "../CreateServerModal/CreateServerModal";
import { AuthContext } from "../../../providers/AuthProvider";

export default function ServerNav() {
	const [isCreateServerOpen, setIsCreateServerOpen] = useState(null);
	const [badgePrivateMessageCount, setBadgePrivateMessageCount] = useState(0);
	const { servers, mutateServers, selectedServer } = useContext(AppContext);
	const { friendShips } = useContext(AppContext);
	const { user } = useContext(AuthContext);

	useEffect(() => {
		// Ajoute un badge en haut a gauche si une demande d'ami est présente
		if (friendShips) {
			setBadgePrivateMessageCount(
				friendShips.filter(
					(friendShip) =>
						user.id == friendShip.addressee_id && friendShip.status == "pending"
				).length
			);
		}
	}, [friendShips]);

	return (
		<ServerNavWrapper>
			{/* Message privée */}
			<Tooltip
				title="Messages privés"
				placement="right"
				overlayClassName="server-popover"
				color="#18191c"
			>
				<Link href="/app/channels/@me">
					<Badge
						count={badgePrivateMessageCount}
						size={"small"}
						offset={[-11, 37]}
					>
						<div className="server-container">
							<div
								className={
									(selectedServer == "@me" ? "server-selected " : "") +
									"server private-message"
								}
							>
								<img
									src="/assets/logo/logo.png"
									alt="logo"
									className="server-logo"
								/>
							</div>
						</div>
					</Badge>
				</Link>
			</Tooltip>

			<div className="serverNav-divider"></div>

			{servers.map((server, i) => {
				return (
					<Tooltip
						title={server.name}
						placement="right"
						overlayClassName="server-popover"
						color="#18191c"
						key={"server" + server.id}
					>
						<Link href={"/app/channels/" + server.id + "/channel"}>
							<div className="server-container">
								<div
									className={
										(selectedServer?.id == server.id
											? "server-selected "
											: "") + "server"
									}
								>
									{server.picture ? (
										<img
											src={process.env.NEXT_PUBLIC_BACK_URL + server.picture}
											alt="logo"
											className="server-logo"
										/>
									) : (
										<p className="server-no-logo">{server.short_name}</p>
									)}
								</div>
							</div>
						</Link>
					</Tooltip>
				);
			})}

			<Tooltip
				title="Ajouter un serveur"
				placement="right"
				overlayClassName="server-popover"
				color="#18191c"
			>
				<div
					className="server add-server"
					onClick={() => {
						setIsCreateServerOpen(true);
					}}
				>
					<FontAwesomeIcon icon={faPlus} />
				</div>
			</Tooltip>

			{/* Modal de créations de serveur */}
			<CreateServerModal
				{...{ isCreateServerOpen, setIsCreateServerOpen, mutateServers }}
			/>
		</ServerNavWrapper>
	);
}
