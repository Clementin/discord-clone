import styled from "styled-components";

export const HeaderDividerWrapper = styled.div`
	position: fixed;
	top: 48px;
	left: 72px;
	z-index: 10;
	height: 1px;
	width: calc(100vw - 72px);
	pointer-events: none;
	background: rgba(0, 0, 0, 0.3);
`;
