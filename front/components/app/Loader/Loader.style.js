import styled from "styled-components";

export const AppLoaderWrapper = styled.div`
	z-index: 999999999;
	height: 100vh;
	width: 100vw;
	background: #26262e;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	position: fixed;
	transition: 0.7s;
	opacity: 1;

	&.disabled {
		background: transparent;
		opacity: 0;

		#loader {
			display: none;
		}
	}

	#loader {
		background: transparent;
		width: 200px;
	}

	p {
		color: white;
		font-size: 16px;
		margin-top: -40px;
	}
`;
