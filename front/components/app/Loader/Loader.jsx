import { useEffect } from "react";
import { AppLoaderWrapper } from "./Loader.style";

export default function AppLoader({ isDomLoaded, isSocketConnected }) {
	useEffect(() => {
		let loader = document.getElementById("app-loader");

		if (loader && isDomLoaded && isSocketConnected) {
			setTimeout(() => {
				loader.style.display = "none";
			}, 700);
		} else if (loader) {
			loader.style.display = "flex";
		}
	}, [isDomLoaded, isSocketConnected]);

	return (
		<AppLoaderWrapper
			id="app-loader"
			className={isDomLoaded && isSocketConnected ? "disabled" : "active"}
		>
			<video
				src="/assets/logo/logo-rotate.mp4"
				id="loader"
				autoPlay
				muted="muted"
				loop
				playsInline
			/>
		</AppLoaderWrapper>
	);
}
