import styled from "styled-components";

export const PrivateMessagesWrapper = styled.div`
	position: absolute;
	width: 240px;
	left: 72px;
	top: 0px;
	padding: 8px;
	min-width: 240px;
	max-width: 240px;
	background-color: #2f3136;
	display: flex;
	flex-direction: column;
	height: calc(100vh - 52px);

	.btn-search {
		outline: none;
		border: none;
		color: #a3a6aa;
		background: #202225;
		border-radius: 5px;
		padding: 3px 0px;
		cursor: pointer;
		margin-bottom: 8px;
	}

	.section-link {
		margin-top: 15px;
		margin-bottom: 20px;

		.link-container {
			a {
				color: white;

				font-size: 16px;
				display: flex;
				background: #42464d;
				padding: 8px;
				border-radius: 5px;
				align-items: center;

				&:hover {
					background: rgba(79, 84, 92, 0.4);
				}

				img {
					width: 24px;
					height: 24px;
					margin-right: 15px;
				}
			}
		}
	}

	.private-message-container {
		padding: 0px 10px;

		.private-message-title {
			width: 100%;
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: space-between;
			margin-bottom: 20px;

			h3 {
				color: #96989d;
				text-transform: uppercase;
				font-size: 11px;
				font-weight: 600;
				width: max-content;
				margin: 0px;
			}

			.fa-plus {
				color: #96989d;
				cursor: pointer;

				&:hover {
					color: hsl(210, calc(1 * 2.9%), 86.7%);
				}
			}

			&:hover {
				h3 {
					color: hsl(210, calc(1 * 2.9%), 86.7%);
				}
			}
		}

		.messages-skeleton-list {
			display: flex;
			flex-direction: column;
			gap: 15px;

			.skeleton {
				display: flex;
				align-items: center;

				.circle {
					width: 32px;
					border-radius: 50%;
					height: 32px;
					background: hsl(220, calc(1 * 7.7%), 22.9%);
					margin-right: 5px;
				}

				.rect {
					width: 144px;
					height: 20px;
					border-radius: 10px;
					background: hsl(220, calc(1 * 7.7%), 22.9%);
				}
			}
		}
	}
`;
