import { PrivateMessagesWrapper } from "./PrivateMessages.style";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { Badge, Tooltip } from "antd";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "../../../../providers/AppProvider";
import { AuthContext } from "../../../../providers/AuthProvider";

export default function PrivateMessages() {
	const [badgeFriendCount, setBadgeFriendCount] = useState(0);
	const { friendShips } = useContext(AppContext);
	const { user } = useContext(AuthContext);

	useEffect(() => {
		// Compteur pour le badge
		if (friendShips) {
			setBadgeFriendCount(
				friendShips.filter(
					(friendShip) =>
						user.id == friendShip.addressee_id && friendShip.status == "pending"
				).length
			);
		}
	}, [friendShips]);

	return (
		<PrivateMessagesWrapper>
			<button className="btn-search">Rechercher/lancer une conversa...</button>

			<div className="section-link">
				<div className="link-container">
					<Badge count={badgeFriendCount} size={"small"} offset={[-20, 20]}>
						<Link href="/app/channels/@me">
							<img src="/assets/img/FriendList/user_hand.svg" alt="" />
							<div>Amis</div>
						</Link>
					</Badge>
				</div>
			</div>

			<div className="private-message-container">
				<div className="private-message-title">
					<h3>messages privés</h3>

					<Tooltip
						title="Créer un MP"
						placement="top"
						overlayClassName="server-popover"
						color="#18191c"
					>
						<FontAwesomeIcon icon={faPlus} />
					</Tooltip>
				</div>

				<div className="messages-skeleton-list">
					{(() => {
						let skeletons = [];
						for (let i = 10; i > 0; i--) {
							skeletons.push(
								<div
									className="skeleton"
									key={"skeleton" + i}
									style={{ opacity: i / 10 }}
								>
									<div className="circle"></div>
									<div className="rect"></div>
								</div>
							);
						}

						return skeletons;
					})()}
				</div>
			</div>
		</PrivateMessagesWrapper>
	);
}
