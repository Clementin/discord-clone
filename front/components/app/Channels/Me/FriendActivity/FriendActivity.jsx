import { FriendActivityWrapper } from "./FriendActivity.style";

export default function FriendActivity() {
	return (
		<FriendActivityWrapper>
			<h2>En ligne</h2>

			<h3>Tout est calme... pour le moment</h3>
			<p>
				Quand un ami commencera une activité, comme jouer à un jeu ou passer du
				temps sur le chat vocal, ce sera affiché ici !
			</p>
		</FriendActivityWrapper>
	);
}
