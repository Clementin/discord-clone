import styled from "styled-components";

export const FriendActivityWrapper = styled.div`
	background-color: #36393f;
	display: flex;
	flex-direction: column;
	position: fixed;
	max-width: 420px;
	width: 100%;
	min-width: 360px;
	padding: 16px;
	height: calc(100vh - 48px);
	top: 48px;
	right: 0px;
	border-left: 1px solid rgba(79, 84, 92, 0.48);

	h2 {
		color: white;
		font-size: 20px;
		font-weight: 600;
		margin-bottom: 20px;
	}

	h3 {
		color: white;
		font-size: 16px;
		font-weight: 600;
		text-align: center;
		width: 100%;
		margin-bottom: 3px;
	}

	p {
		color: #b9bbbe;
		text-align: center;
		line-height: 1;
		font-size: 14px;
		max-width: 293px;
		text-align: center;
		margin: 0px auto;
	}

	@media (max-width: 1200px) {
		display: none;
	}
`;
