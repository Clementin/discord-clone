import { Button } from "antd";

export default function EmptyFriendShip({
	img,
	text,
	button = false,
	setTabOpen,
}) {
	return (
		<div className="empty-container">
			<img src={img} alt="" />
			<p>{text}</p>

			{button ? (
				<Button
					type="primary"
					onClick={() => {
						setTabOpen(button.link);
					}}
				>
					{button.text}
				</Button>
			) : null}
		</div>
	);
}

export function EmptyOnline() {
	return (
		<EmptyFriendShip
			img="/assets/img/FriendList/empty-online.svg"
			text="Il n'y a personne dans les parages pour jouer avec Wumpus."
		/>
	);
}

export function EmptyWaiting() {
	return (
		<EmptyFriendShip
			img="/assets/img/FriendList/empty-waiting.svg"
			text="Il n'y a aucune demande d'ami en attente. Tiens, voila Wumpus
en attendant."
		/>
	);
}

export function EmptyBlocked() {
	return (
		<EmptyFriendShip
			img="/assets/img/FriendList/empty-blocked.svg"
			text="Tu ne peut pas débloquer le Wumpus."
		/>
	);
}

export function EmptyAll({ setTabOpen }) {
	return (
		<EmptyFriendShip
			img="/assets/img/FriendList/empty-all.svg"
			text="Wumpus attend des amis. Mais rien ne t'oblige à en ajouter !"
			button={{ link: "add", text: "Ajouter un ami" }}
			{...{ setTabOpen }}
		/>
	);
}
