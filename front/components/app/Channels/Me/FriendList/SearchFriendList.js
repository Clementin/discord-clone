import slugify from "react-slugify";

// Search user who correspond to string
export function search(string, friendShips, setter) {
	// if input is empty return all friendShips
	if (!string) {
		return setter(friendShips);
	}

	let tempFriendShips = [];
	for (let friendShip of friendShips) {
		// otherUser == not Auth::user
		if (slugify(friendShip.otherUser.nickname).includes(slugify(string))) {
			tempFriendShips.push(friendShip);
		}
	}

	return setter(tempFriendShips);
}
