import { Form, Input } from "antd";
import { useEffect, useState } from "react";
import { FriendListWrapper } from "./FriendList.style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { search } from "./SearchFriendList";
import {
	EmptyAll,
	EmptyBlocked,
	EmptyOnline,
	EmptyWaiting,
} from "./EmptyFriendShip";
import ListContainer from "./ListContainer";

export default function FriendList({
	tabOpen,
	setTabOpen,
	filteredTabFriendShip,
}) {
	const [filteredSearchFriendShip, setFilteredSearchFriendShip] = useState([]);

	const [form] = Form.useForm();

	// On startup show all friendShip
	useEffect(() => {
		setFilteredSearchFriendShip(filteredTabFriendShip);
	}, [filteredTabFriendShip]);

	// Reset SearchBar on tabOpen change
	useEffect(() => {
		form.resetFields();
	}, [tabOpen]);

	// Composant pour la recherche
	function SearchBar() {
		return (
			<Form form={form}>
				<Form.Item name={"nickname"}>
					<Input
						placeholder="Rechercher"
						onInput={(e) => {
							search(
								e.target.value,
								filteredTabFriendShip,
								setFilteredSearchFriendShip
							);
						}}
						className="friendship-search-input"
						suffix={<FontAwesomeIcon icon={faSearch} />}
					/>
				</Form.Item>
			</Form>
		);
	}

	return (
		<FriendListWrapper>
			{filteredTabFriendShip?.length ? (
				<div className="container">
					{SearchBar()}
					<ListContainer
						frienships={filteredSearchFriendShip}
						setFriendships={setFilteredSearchFriendShip}
						tab={tabOpen}
						{...{ setTabOpen }}
					/>
				</div>
			) : tabOpen == "online" ? (
				<EmptyOnline />
			) : tabOpen == "all" ? (
				<EmptyAll {...{ setTabOpen }} />
			) : tabOpen == "pending" ? (
				<EmptyWaiting />
			) : (
				<EmptyBlocked />
			)}
		</FriendListWrapper>
	);
}
