import {
	faCheck,
	faClose,
	faEllipsisVertical,
	faMessage,
	faUserXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Modal, Tooltip } from "antd";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "../../../../../providers/AppProvider";
import { AuthContext } from "../../../../../providers/AuthProvider";
import CrudService from "../../../../../services/CrudService";
import { emitFriendStatusUpdate } from "../../../../../socket/handler/FriendHandler";
import {
	ActionPopoverAddNote,
	ActionPopoverBlock,
	ActionPopoverCall,
	ActionPopoverCopy,
	ActionPopoverDivider,
	ActionPopoverProfil,
	ActionPopoverRemove,
	ActionPopoverSendMessage,
	ActionPopoverUnblock,
	ActionPopoverVideoCall,
	ActionPopoverVoiceCall,
} from "../../../../global/ActionPopover/ActionPopoverItems";
import { EmptyAll, EmptyBlocked, EmptyWaiting } from "./EmptyFriendShip";

export default function ListContainer({
	frienships,
	setFriendships,
	tab,
	setTabOpen,
}) {
	const router = useRouter();
	const [modalInformation, setModalInformation] = useState({ open: false });
	const {
		setUserProfileModalInformations,
		setActionPopoverContent,
		mutateFriendShips,
	} = useContext(AppContext);
	const { user } = useContext(AuthContext);

	// Supprime une amitié entre 2 personnes / Débloque un utilisateur bloqué
	const handleRemoveFriendShip = (friendShip) => {
		// front hide
		setFriendships(
			frienships.filter((item) => (item.id != friendShip.id ? true : false))
		);

		CrudService.delete("friend_ship", friendShip.id).then((response) => {
			if (response.status) {
				mutateFriendShips();

				// Envoie de la notification a l'utilisateur
				emitFriendStatusUpdate(friendShip.otherUser.id);
			}
		});
	};

	// Accepte l'amitié entre les deux utilisateurs
	const handleAcceptFriendShip = (friendShip) => {
		// front hide
		setFriendships(
			frienships.filter((item) => (item.id != friendShip.id ? true : false))
		);

		CrudService.update("friend_ship", {}, friendShip.id).then((response) => {
			if (response.status) {
				mutateFriendShips();

				// Envoie de la notification a l'utilisateur
				emitFriendStatusUpdate(friendShip.otherUser.id);
			}
		});
	};

	return frienships ? (
		<div className="list-container">
			<h4 className="friendship-number">
				{tab == "pending"
					? "En attente"
					: tab == "blocked"
					? "Bloqués"
					: tab == "all"
					? "Tous les amis"
					: null}{" "}
				- {frienships.length}
			</h4>
			<div className="spacing"></div>

			<div className="friendships">
				{frienships.length ? (
					frienships.map((friendShip, i) => {
						return (
							<div
								className="friendship"
								key={i}
								onClick={(e) => {
									if (tab == "all" || tab == "online") {
										router.push("/app/channels/@me/" + friendShip.otherUser.id);
									} else if (tab == "pending" || tab == "blocked") {
										setUserProfileModalInformations({
											user: friendShip.otherUser,
											friendShip: friendShip,
										});
									}
								}}
								onContextMenu={(e) => {
									// Désactive le menu de contexte
									e.preventDefault();

									// Blocked
									if (tab == "blocked") {
										setActionPopoverContent({
											positionX: e.clientX,
											positionY: e.clientY,
											actions: [
												ActionPopoverProfil(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverSendMessage(
													false,
													false,
													friendShip.otherUser.id,
													router
												),
												ActionPopoverCall(false, true),
												ActionPopoverAddNote(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverDivider(),
												ActionPopoverUnblock(
													false,
													false,
													friendShip,
													null,
													mutateFriendShips
												),
												ActionPopoverDivider(),
												ActionPopoverCopy(
													false,
													false,
													friendShip.otherUser.nickname
												),
											],
										});
									} else if (tab == "pending") {
										setActionPopoverContent({
											positionX: e.clientX,
											positionY: e.clientY,
											actions: [
												ActionPopoverProfil(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverSendMessage(
													false,
													false,
													friendShip.otherUser.id,
													router
												),
												ActionPopoverCall(),
												ActionPopoverAddNote(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverDivider(),
												ActionPopoverBlock(),
												ActionPopoverDivider(),
												ActionPopoverCopy(
													false,
													false,
													friendShip.otherUser.nickname
												),
											],
										});
									} else {
										setActionPopoverContent({
											positionX: e.clientX,
											positionY: e.clientY,
											actions: [
												ActionPopoverProfil(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverSendMessage(
													false,
													false,
													friendShip.otherUser.id,
													router
												),
												ActionPopoverCall(),
												ActionPopoverAddNote(
													false,
													false,
													setUserProfileModalInformations,
													friendShip.otherUser,
													friendShip
												),
												ActionPopoverDivider(),
												ActionPopoverRemove(
													false,
													false,
													friendShip,
													null,
													null,
													mutateFriendShips
												),
												ActionPopoverBlock(),
												ActionPopoverDivider(),
												ActionPopoverCopy(
													false,
													false,
													friendShip.otherUser.nickname
												),
											],
										});
									}
								}}
							>
								<div className="profile-pic-container">
									{friendShip.otherUser.picture ? (
										<img
											className="user-pic"
											src="https://cdn.discordapp.com/avatars/132076642133737472/23b60c648a1417e0fb8fe3664d883044.webp?size=32"
											alt="user-pic"
										/>
									) : (
										<div
											className="user-pic-default"
											style={{ backgroundColor: friendShip.otherUser.color }}
										>
											<img src="/assets/logo/logo.png" alt="logo" />
										</div>
									)}
								</div>

								<div className="nickname-container">
									<div className="nickname">
										{friendShip.otherUser.nickname}
									</div>

									<p className="id">
										{tab == "pending"
											? user.id == friendShip.requester_id
												? "Demande d'ami envoyée"
												: "Demande d'ami reçue"
											: tab == "blocked"
											? "Bloqué"
											: null}
									</p>
								</div>

								{/* Partie de droite de la ligne avec différent bouton */}
								<div className="action-container">
									{tab == "pending" ? (
										<>
											{/* Boutton d'acceptation */}
											{user.id != friendShip.requester_id ? (
												<Tooltip
													placement="top"
													title="Accepter"
													overlayClassName="server-popover"
												>
													<button
														className="button accept"
														onClick={(e) => {
															e.stopPropagation();
															handleAcceptFriendShip(friendShip);
														}}
													>
														<FontAwesomeIcon icon={faCheck} />
													</button>
												</Tooltip>
											) : (
												""
											)}

											{/* Boutton d'annulation */}
											<Tooltip
												placement="top"
												title="Annuler"
												overlayClassName="server-popover"
											>
												<button
													className="button cancel"
													onClick={(e) => {
														e.stopPropagation();
														handleRemoveFriendShip(friendShip);
													}}
												>
													<FontAwesomeIcon icon={faClose} />
												</button>
											</Tooltip>
										</>
									) : tab == "blocked" ? (
										<Tooltip
											placement="top"
											title="Débloquer"
											overlayClassName="server-popover"
										>
											<button
												className="button cancel"
												onClick={(e) => {
													e.stopPropagation();
													handleRemoveFriendShip(friendShip);
												}}
											>
												<FontAwesomeIcon icon={faUserXmark} />
											</button>
										</Tooltip>
									) : tab == "all" || tab == "online" ? (
										<>
											<Tooltip
												placement="top"
												title="Envoyer un MP"
												overlayClassName="server-popover"
											>
												<button
													className="button"
													onClick={() => {
														router.push(
															"/app/channels/@me/" + friendShip.otherUser.id
														);
													}}
												>
													<FontAwesomeIcon icon={faMessage} />
												</button>
											</Tooltip>

											<Tooltip
												placement="top"
												title="Plus"
												overlayClassName="server-popover"
											>
												<button
													className="button"
													onClick={(e) => {
														e.stopPropagation();

														setActionPopoverContent({
															positionX: e.clientX,
															positionY: e.clientY,
															actions: [
																ActionPopoverVideoCall(),
																ActionPopoverVoiceCall(),
																ActionPopoverRemove(
																	true,
																	false,
																	friendShip,
																	() => {
																		handleRemoveFriendShip(friendShip);
																	}
																),
															],
														});
													}}
												>
													<FontAwesomeIcon icon={faEllipsisVertical} />
												</button>
											</Tooltip>
										</>
									) : null}
								</div>
							</div>
						);
					})
				) : tab == "pending" ? (
					<EmptyWaiting />
				) : tab == "all" ? (
					<EmptyAll {...{ setTabOpen }} />
				) : tab == "blocked" ? (
					<EmptyBlocked />
				) : null}
			</div>

			<Modal
				className="modal-information align-left no-text-transform title-bigger title-bold"
				closable={false}
				centered
				destroyOnClose
				maskClosable
				footer={null}
				open={modalInformation.open}
				maskStyle={{
					background: "rgba(0,0,0,0.85)",
				}}
				onCancel={() => {
					setModalInformation({ open: false });
				}}
			>
				<div className="modal-container">
					<h2>{modalInformation.title}</h2>
					<p>{modalInformation.message}</p>
				</div>

				<div className="modal-footer">{modalInformation.footer}</div>
			</Modal>
		</div>
	) : null;
}
