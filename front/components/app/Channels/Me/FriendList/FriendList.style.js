import styled from "styled-components";

export const FriendListWrapper = styled.div`
	position: fixed;
	background: #36393f;
	top: 48px;
	left: calc(72px + 240px);
	height: calc(100vh - 48px);
	width: calc(100vw - 72px - 240px - 420px);

	.empty-container {
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		color: #a3a6aa;
		font-size: 16px;
		display: flex;
		flex-direction: column;

		p {
			text-align: center;
			margin: 50px 0px 0px 0px;
		}

		button {
			margin: 20px auto 0px auto;
			height: max-content;
			border-radius: 5px;
			padding: 8px 16px;
		}
	}

	.container {
		font-size: 16px;
		display: flex;
		flex-direction: column;
		width: 100%;
		height: 100%;
		padding: 16px 0px 16px 0px;

		.ant-form {
			padding: 0px 20px 0px 30px;

			.friendship-search-input {
				background: #202225;
				color: #a3a6aa;
				font-weight: 500;
				border: none;
				border-radius: 3px;
				box-shadow: none;
				height: max-content;
				padding: 5px 10px 5px 10px;

				input {
					background: #202225;
					color: #a3a6aa;
					font-size: 16px;

					&::placeholder {
						color: #a3a6aa;
					}
				}
			}
		}

		/* 20 / 30 */

		.list-container {
			display: flex;
			flex-direction: column;

			.friendship-number {
				color: #b9bbbe;
				text-transform: uppercase;
				font-size: 12px;
				font-weight: 600;
				padding: 0px 20px 0px 30px;
			}

			.spacing {
				width: 100%;
				height: 1px;
				margin-top: 10px;
			}

			.friendships {
				display: flex;
				flex-direction: column;
				overflow-x: hidden;
				overflow-y: scroll;
				padding: 0px 15px 0px 30px;
				margin-right: 5px;
				height: calc(100vh - 162px);

				&::-webkit-scrollbar {
					width: 7px;
					/* 1 */
				}
				::-webkit-scrollbar-button {
					/* 2 */
					/* background: #202225; */
					display: none;
				}
				::-webkit-scrollbar-track {
					/* 3 */
					background: #2e3338;
					border-radius: 10px;
				}
				::-webkit-scrollbar-track-piece {
					/* 4 */
					display: none;
				}
				::-webkit-scrollbar-thumb {
					/* 5 */
					background: #202225;
					border-radius: 10px;
				}
				::-webkit-scrollbar-corner {
					/* 6 */
					display: none;
				}
				::-webkit-resizer {
					display: none;
				}

				.friendship {
					display: flex;
					flex-direction: row;
					align-items: center;
					height: 62px;
					min-height: 62px;
					border-top: 1px solid #42464d;
					border-radius: 7px;
					padding: 0px 10px;
					cursor: pointer;

					&:hover {
						background: #40444b;
					}

					.profile-pic-container {
						display: flex;
						height: 32px;
						width: 32px;
						margin-right: 10px;

						.user-pic {
							border-radius: 50%;
							object-fit: cover;
						}

						.user-pic-default {
							border-radius: 50%;
							width: 32px;
							position: relative;
							max-width: 32px;

							img {
								position: absolute;
								top: 47%;
								left: 50%;
								transform: translate(-50%, -50%);

								/* object-fit: cover; */
								width: 95%;
								padding: 6px;
							}
						}
					}

					.nickname-container {
						display: flex;
						flex-direction: column;

						p {
							margin: 0px;
							line-height: 1;
						}

						.nickname {
							color: white;
							font-weight: 600;
							margin: 0px;
							font-size: 16px;
						}

						.id {
							color: #b9bbbe;
							font-size: 12px;
						}
					}

					.action-container {
						display: flex;
						margin-left: auto;
						gap: 0px 10px;

						.button {
							background: #2f3136;
							outline: none;
							border: none;
							height: 36px;
							width: 36px;
							border-radius: 50%;
							cursor: pointer;

							svg {
								color: #b9bbbe;
								font-size: 14px;
							}

							&:hover {
								svg {
									color: white;
								}
							}

							&.cancel {
								&:hover {
									svg {
										color: #ed4245;
									}
								}
							}

							&.accept {
								&:hover {
									svg {
										color: var(--sucess);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	@media (max-width: 1200px) {
		width: calc(100vw - 72px - 240px);
	}
`;
