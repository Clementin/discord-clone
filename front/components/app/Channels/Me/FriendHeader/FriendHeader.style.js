import styled from "styled-components";

export const FriendHeaderWrapper = styled.div`
	background: #36393f;
	height: 48px;
	width: calc(100vw - 72px - 240px);
	position: fixed;
	top: 0px;
	left: calc(72px + 240px);
	display: flex;
	flex-direction: row;
	align-items: center;
	padding: 0px 8px;

	.friend-ico {
		margin-left: 10px;
	}

	.p-side-friend-ico {
		color: white;
		font-weight: 500;
		font-size: 16px;
		margin: 0px 7px 0px 10px;
	}

	.divider {
		width: 1px;
		height: 24px;
		margin: 0 8px;
		background: rgba(79, 84, 92, 0.48);
	}

	.ant-menu {
		border: none;
		background: transparent;
		line-height: unset;

		.ant-menu-item {
			padding: 0px;
			margin: 0px 8px;
			height: max-content;

			&:first-of-type {
				margin: 0px 8px;
			}

			.ant-menu-title-content {
				color: #b9bbbe;
				padding: 2px 10px;
				border-radius: 5px;
				transition: none;
				font-size: 15px;
			}

			&.ant-menu-item-active {
				.ant-menu-title-content {
					background: #454950;
					color: #b9bbbe;
					padding: 2px 10px;
					border-radius: 5px;
				}

				&::after {
					display: none;
				}
			}

			&.ant-menu-item-selected {
				.ant-menu-title-content {
					color: white;
					background: #454950;
					padding: 2px 10px;
					border-radius: 5px;
					cursor: default;
				}

				&::after {
					display: none;
				}
			}

			&.add {
				.ant-menu-title-content {
					background: #2d7d46;
					color: white;
				}

				&.ant-menu-item-selected {
					.ant-menu-title-content {
						background: transparent;
						color: #46c466;
					}
				}
			}
		}
	}

	.right-actions-container {
		display: flex;
		margin-left: auto;
		padding: 0px 10px;
		gap: 16px;

		/* FontAwesomeIcon */
		svg {
			width: 20px;
			height: 20px;
			cursor: pointer;

			path {
				fill: #b5bac1;
			}

			&:hover {
				path {
					fill: #caced4;
				}
			}
		}

		.divider {
			width: 1px;
			height: 24px;
			margin: 0px;
			background: #4e50587a;
		}
	}
`;
