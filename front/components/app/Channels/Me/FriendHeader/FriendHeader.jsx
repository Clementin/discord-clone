import { FriendHeaderWrapper } from "./FriendHeader.style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleQuestion, faInbox } from "@fortawesome/free-solid-svg-icons";
import { Badge, Menu, Tooltip } from "antd";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../../providers/AuthProvider";

export default function FriendHeader({ tabOpen, setTabOpen, friendShips }) {
	const [badgePendingCount, setBadgePendingCount] = useState(0);
	const { user } = useContext(AuthContext);

	useEffect(() => {
		// Compteur pour le badge
		if (friendShips) {
			setBadgePendingCount(
				friendShips.filter(
					(friendShip) =>
						user.id == friendShip.addressee_id && friendShip.status == "pending"
				).length
			);
		}
	}, [friendShips]);

	return (
		<FriendHeaderWrapper>
			<img
				className="friend-ico"
				src="/assets/img/FriendList/user_hand_grey.svg"
				alt=""
			/>
			<p className="p-side-friend-ico">Amis</p>

			<div className="divider"></div>

			<Menu
				onSelect={({ key }) => {
					setTabOpen(key);
				}}
				selectedKeys={tabOpen}
				defaultSelectedKeys={
					friendShips && friendShips.length ? "online" : "add"
				}
				mode="horizontal"
				items={[
					{ label: "En ligne", key: "online" },
					{ label: "Tous", key: "all" },
					{
						label: (
							<span style={{ display: "inline-block" }}>
								<span>En attente</span>
								<Badge
									size="small"
									count={badgePendingCount}
									offset={[5, -1]}
								/>
							</span>
						),
						key: "pending",
					},
					{ label: "Bloqué", key: "blocked" },
					{ label: "Ajouter un ami", key: "add", className: "add" },
				]}
			/>

			<div className="right-actions-container">
				{/* Nouveau groupe privé */}
				<Tooltip
					title="Nouveau groupe privé"
					placement="bottom"
					color="#18191c"
					overlayClassName="server-popover"
				>
					<svg aria-hidden="true" role="img" viewBox="0 0 24 24">
						<path d="M20.998 0V3H23.998V5H20.998V8H18.998V5H15.998V3H18.998V0H20.998ZM2.99805 20V24L8.33205 20H14.998C16.102 20 16.998 19.103 16.998 18V9C16.998 7.896 16.102 7 14.998 7H1.99805C0.894047 7 -0.00195312 7.896 -0.00195312 9V18C-0.00195312 19.103 0.894047 20 1.99805 20H2.99805Z"></path>
					</svg>
				</Tooltip>

				<div className="divider"></div>

				{/* Boite de reception */}
				<Tooltip
					title="Boîte de réception"
					placement="bottom"
					color="#18191c"
					overlayClassName="server-popover"
				>
					<FontAwesomeIcon icon={faInbox} />
				</Tooltip>

				{/* Bouton d'aide */}
				<Tooltip
					title="Aide"
					placement="bottom"
					color="#18191c"
					overlayClassName="server-popover"
				>
					<a
						href="https://support.discord.com/hc/fr"
						rel="noopener noreferrer"
						target="_blank"
					>
						<FontAwesomeIcon icon={faCircleQuestion} />
					</a>
				</Tooltip>
			</div>
		</FriendHeaderWrapper>
	);
}
