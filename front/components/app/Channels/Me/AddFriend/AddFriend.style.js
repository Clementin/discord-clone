import styled from "styled-components";

export const AddFriendWrapper = styled.div`
	position: fixed;
	background: #36393f;
	top: 48px;
	left: calc(72px + 240px);
	height: calc(100vh - 48px);
	width: calc(100vw - 72px - 240px - 420px);
	padding: 20px 0px;

	h2 {
		text-transform: uppercase;
		color: white;
		font-size: 16px;
		padding: 0px 30px;
	}

	p:first-of-type {
		color: #a3a6aa;
		padding: 0px 30px;
	}

	.ant-form {
		border-bottom: 1px solid rgba(79, 84, 92, 0.48);
		padding: 0px 30px;
		padding-bottom: 20px;
		position: relative;

		.ant-form-item {
			margin: 0px;

			.ant-form-item-control-input {
				border-radius: 8px;
			}

			input {
				background: #202225;
				font-size: 16px;
				padding: 12px 16px;
				height: max-content;
				border: none;
				border-radius: 8px;
				color: white;

				&::placeholder {
					opacity: 0.6;
				}
			}
		}

		button {
			height: max-content;
			border-radius: 5px;
			padding: 6px 20px;
			border: none;
			/* top: 50%; */
			top: 35px;
			right: 0px;
			position: absolute;
			right: 40px;
			transform: translate(0%, -80%);

			&.disabled {
				cursor: not-allowed;
				background: #3c438b;
				color: grey;

				&:hover {
					background: #3c438b;
				}
			}
		}

		#input-submit-response {
			display: none;
			margin: 5px 0px 0px 0px;
			padding: 0px;
			font-size: 14px;
		}

		&.form-error {
			input {
				border: 1px solid hsl(359, calc(1 * 82.6%), 59.4%);
			}

			#input-submit-response {
				display: inline-block;
				color: #f38688;
			}
		}

		&.form-success {
			input {
				border: 1px solid hsl(139, calc(1 * 47.3%), 43.9%);
			}

			#input-submit-response {
				display: inline-block;
				color: #46c46e;
			}
		}
	}
`;
