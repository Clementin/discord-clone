import { AddFriendWrapper } from "./AddFriend.style";
import { Button, Form, Input, Modal } from "antd";
import CrudService from "/services/CrudService";
import { useContext, useState } from "react";
import { AppContext } from "../../../../../providers/AppProvider";
import { emitFriendStatusUpdate } from "../../../../../socket/handler/FriendHandler";

export default function AddFriend() {
	const [form] = Form.useForm();
	const nicknameWatcher = Form.useWatch("nickname", form);
	const [modalInformation, setModalInformation] = useState({ open: false });
	const { mutateFriendShips } = useContext(AppContext);

	const handleSubmit = (data) => {
		if (nicknameWatcher) {
			CrudService.create("friend_ship", data).then((response) => {
				let formAddFriend = document.getElementById("form-add-friend");

				if (response.status) {
					mutateFriendShips();
					form.resetFields();
					formAddFriend.className = "ant-form form-success";
					document.getElementById("input-submit-response").innerHTML =
						response.message;

					// Envoie de la notification a l'utilisateur
					emitFriendStatusUpdate(response.friendShip.addressee_id);
				} else {
					setModalInformation({
						open: true,
						title: "échec de l'envoi de la demande d'ami",
						message: response.message,
					});

					// Affichage des érreurs en front
					formAddFriend.className = "ant-form form-error";
					document.getElementById("input-submit-response").innerHTML =
						response.message;
				}
			});
		}
	};

	return (
		<AddFriendWrapper>
			<h2>ajouter un ami</h2>
			<p>
				Tu peux ajouter un ami grâce à son discord Tag. Attention aux
				mAjUsCuLeS&nbsp;!
			</p>

			<Form form={form} onFinish={handleSubmit} id="form-add-friend">
				<Form.Item name={"nickname"}>
					<Input maxLength={37} placeholder="Entre un nom d'utilisateur#0000" />
				</Form.Item>
				<p id="input-submit-response"></p>

				<Button
					type="primary"
					htmlType="submit"
					className={
						!nicknameWatcher || !nicknameWatcher.length ? "disabled" : ""
					}
				>
					Envoyer une demande d'ami
				</Button>
			</Form>

			{/* Modale d'affichage de message d'erreur */}
			<Modal
				className="modal-information"
				closable={false}
				centered
				destroyOnClose
				maskClosable
				footer={null}
				open={modalInformation.open}
				maskStyle={{
					background: "rgba(0,0,0,0.85)",
				}}
				onCancel={() => {
					setModalInformation({ open: false });
				}}
			>
				<div className="modal-container">
					<h2>{modalInformation.title}</h2>
					<p>{modalInformation.message}</p>
				</div>

				<div className="modal-footer">
					<Button
						type="primary"
						onClick={() => {
							setModalInformation({ open: false });
						}}
					>
						OK
					</Button>
				</div>
			</Modal>
		</AddFriendWrapper>
	);
}
