import { Form, Menu, Skeleton } from "antd";
import moment from "moment";
import { useCallback, useContext, useEffect, useState } from "react";
import { UserProfileModalWrapper } from "./UserProfileModal.style";
import { AuthContext } from "../../../providers/AuthProvider";
import CrudService from "../../../services/CrudService";
import { debounce } from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import { AppContext } from "../../../providers/AppProvider";
import {
	ActionPopoverBlock,
	ActionPopoverCopy,
	ActionPopoverDivider,
	ActionPopoverRemove,
	ActionPopoverSendMessage,
	ActionPopoverUnblock,
} from "../../global/ActionPopover/ActionPopoverItems";
import { emitFriendStatusUpdate } from "../../../socket/handler/FriendHandler";

export default function UserProfileModal({
	profileUser,
	setUserProfileModalInformations,
	initialFriendShip = null,
}) {
	// State pour le menu ( Infos utilisateur / Serveurs en commun / Amis en commun )
	const [tabOpen, setTabOpen] = useState("info");
	const [note, setNote] = useState(null);
	const [friendShip, setFriendship] = useState(initialFriendShip);
	const [form] = Form.useForm();
	const router = useRouter();
	const { user } = useContext(AuthContext);
	const { setActionPopoverContent, mutateFriendShips } = useContext(AppContext);

	useEffect(() => {
		// Récupere la note renseigné par l'utilisateur
		if (profileUser && user) {
			CrudService.get("note", profileUser.id).then((noteResponse) => {
				// Remplie le state
				setNote(noteResponse);
				// Remplie l'input si il y a une note
				form.setFieldsValue({ note: noteResponse.text ?? "" });
			});
		}
	}, [profileUser, user]);

	// Envoie la requette de mise à jour ou création de la note uniquement 500ms apres que l'utilisateur ai finis de taper
	const handleNoteTextChange = useCallback(
		debounce(() => {
			// Mise à jour
			if (note.id) {
				CrudService.update(
					"notes",
					{ text: form.getFieldValue("note") },
					note.id
				);
			}
			// Création
			else {
				CrudService.create("notes", {
					text: form.getFieldValue("note"),
					target_id: profileUser.id,
				}).then((response) => {
					setNote(response.note);
				});
			}
		}, 300),
		[note]
	);

	// Accepte l'amitié entre les deux utilisateurs
	const handleAcceptFriendShip = () => {
		CrudService.update("friend_ship", {}, friendShip.id).then((response) => {
			if (response.status) {
				mutateFriendShips();
				setFriendship(response.friendShip);

				// Envoie de la notification a l'utilisateur
				emitFriendStatusUpdate(friendShip.otherUser.id);
			}
		});
	};

	// Supprime une amitié entre 2 personnes ou la refuse / Débloque un utilisateur bloqué
	const handleRemoveFriendShip = () => {
		setFriendship(null);
		CrudService.delete("friend_ship", friendShip.id).then((response) => {
			mutateFriendShips();

			// Envoie de la notification a l'utilisateur
			emitFriendStatusUpdate(friendShip.otherUser.id);
		});
	};

	// Envoie une demande d'ami
	const handleSendFriendShipRequest = () => {
		CrudService.create("friend_ship", { nickname: profileUser.nickname }).then(
			(response) => {
				if (response.status) {
					mutateFriendShips();
					setFriendship(response.friendShip);

					// Envoie de la notification a l'utilisateur
					emitFriendStatusUpdate(response.friendShip.addressee_id);
				}
			}
		);
	};

	return (
		<UserProfileModalWrapper>
			{/* Header */}
			<div
				className="header"
				style={{
					background:
						"linear-gradient(0deg,#292b2f 41%, " + profileUser.color + " 15%)",
				}}
			>
				{/* Photo de profile */}
				<div className="profile-pic-container">
					{profileUser.picture ? (
						<img
							className="user-pic"
							src="https://cdn.discordapp.com/avatars/132076642133737472/23b60c648a1417e0fb8fe3664d883044.webp?size=32"
							alt="user-pic"
						/>
					) : (
						<div
							className="user-pic-default"
							style={{ backgroundColor: profileUser.color }}
						>
							<img src="/assets/logo/logo.png" alt="logo" />
						</div>
					)}
				</div>

				<div className="action-container">
					{!friendShip ? (
						// Si ils ne sont pas ami
						<>
							<button
								className="action-button green"
								onClick={() => {
									handleSendFriendShipRequest();
								}}
							>
								Envoyer une demande d'ami
							</button>
							<FontAwesomeIcon
								icon={faEllipsisVertical}
								onClick={(e) => {
									setActionPopoverContent({
										positionX: e.clientX,
										positionY: e.clientY,
										actions: [
											ActionPopoverBlock(true),
											ActionPopoverSendMessage(
												false,
												false,
												profileUser.id,
												router,
												setUserProfileModalInformations
											),
											ActionPopoverDivider(),
											ActionPopoverCopy(false, false, profileUser.nickname),
										],
									});
								}}
							/>{" "}
						</>
					) : friendShip.status == "pending" &&
					  friendShip.addressee_id == user.id ? (
						// Est l"utilisateur que l'on a demandé en ami
						<>
							<button
								className="action-button green"
								onClick={() => {
									handleAcceptFriendShip(friendShip);
								}}
							>
								Accepter
							</button>
							<button
								className="action-button"
								onClick={() => {
									handleRemoveFriendShip(friendShip);
								}}
							>
								Refuser
							</button>

							<FontAwesomeIcon
								icon={faEllipsisVertical}
								onClick={(e) => {
									setActionPopoverContent({
										positionX: e.clientX,
										positionY: e.clientY,
										actions: [
											ActionPopoverBlock(true),
											ActionPopoverSendMessage(
												false,
												false,
												profileUser.id,
												router,
												setUserProfileModalInformations
											),
											ActionPopoverDivider(),
											ActionPopoverCopy(false, false, profileUser.nickname),
										],
									});
								}}
							/>
						</>
					) : friendShip.status == "pending" &&
					  friendShip.addressee_id != user.id ? (
						// Est l'utitilisateur qui a fais la demande en ami
						<>
							<button className="action-button green disabled">
								Demande d'ami envoyée
							</button>

							<FontAwesomeIcon
								icon={faEllipsisVertical}
								onClick={(e) => {
									setActionPopoverContent({
										positionX: e.clientX,
										positionY: e.clientY,
										actions: [
											ActionPopoverBlock(true),
											ActionPopoverSendMessage(
												false,
												false,
												profileUser.id,
												router,
												setUserProfileModalInformations
											),
											ActionPopoverDivider(),
											ActionPopoverCopy(false, false, profileUser.nickname),
										],
									});
								}}
							/>
						</>
					) : friendShip.status == "blocked" ? (
						// L'utilisateur a été bloqué
						<>
							<FontAwesomeIcon
								icon={faEllipsisVertical}
								onClick={(e) => {
									setActionPopoverContent({
										positionX: e.clientX,
										positionY: e.clientY,
										actions: [
											ActionPopoverUnblock(
												false,
												false,
												friendShip,
												setFriendship,
												mutateFriendShips
											),
											ActionPopoverDivider(),
											ActionPopoverCopy(false, false, profileUser.nickname),
										],
									});
								}}
							/>
						</>
					) : (
						// Les deux utilisateurs sont déja amis
						<>
							<button
								className="action-button green"
								onClick={() => {
									router.push("/app/channels/@me/" + profileUser.id);
									setUserProfileModalInformations(null);
								}}
							>
								Envoyer un message
							</button>
							{/* Retirer l'ami Bloquer Envoyer un message ----- Copier l'identifiant */}
							<FontAwesomeIcon
								icon={faEllipsisVertical}
								onClick={(e) => {
									setActionPopoverContent({
										positionX: e.clientX,
										positionY: e.clientY,
										actions: [
											ActionPopoverRemove(
												true,
												false,
												friendShip,
												null,
												setFriendship,
												mutateFriendShips
											),
											ActionPopoverBlock(true),
											ActionPopoverSendMessage(
												false,
												false,
												profileUser.id,
												router,
												setUserProfileModalInformations
											),
											ActionPopoverDivider(),
											ActionPopoverCopy(false, false, profileUser.nickname),
										],
									});
								}}
							/>
						</>
					)}
				</div>
			</div>

			{/* Body */}
			<div className="body">
				<span className="user-name">{profileUser.nickname}</span>

				<Menu
					onSelect={({ key }) => {
						setTabOpen(key);
					}}
					selectedKeys={tabOpen}
					defaultSelectedKeys={"info"}
					mode="horizontal"
					items={[
						{ label: "Infos utilisateur", key: "info" },
						{ label: "Serveurs en commun", key: "server" },
						{ label: "Amis en commun", key: "friend" },
					]}
				/>

				{/* Onglet de menu ouvert */}
				{tabOpen == "info" ? (
					<div className="user-informations">
						<h3>Membre discord depuis</h3>
						<span className="created_at">
							{moment(profileUser.created_at).format("MMM. D, YYYY")}
						</span>
						<h3>Note</h3>

						<Form form={form}>
							<Form.Item name={"note"}>
								{note ? (
									<textarea
										onChange={handleNoteTextChange}
										placeholder="Clique pour ajouter une note"
									/>
								) : (
									<Skeleton
										active
										round
										loading
										title={false}
										style={{ marginTop: "10px" }}
									/>
								)}
							</Form.Item>
						</Form>
					</div>
				) : (
					<></>
				)}
			</div>
		</UserProfileModalWrapper>
	);
}
