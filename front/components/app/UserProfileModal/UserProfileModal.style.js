import styled from "styled-components";

export const UserProfileModalWrapper = styled.div`
	min-width: 600px;
	width: 600px;
	max-width: 600px;
	min-height: 586px;
	height: 586px;
	max-height: 586px;
	background: #292b2f;
	border-radius: 10px;
	display: flex;
	flex-direction: column;
	overflow: hidden;

	.header {
		width: 100%;
		display: flex;
		flex-direction: row;
		padding: 30px 15px 0px 15px;
		align-items: flex-end;

		.profile-pic-container {
			display: flex;
			height: 120px;
			width: 120px;
			padding: 7px;
			margin-right: 7px;
			background: #292b2f;
			border-radius: 50%;

			.user-pic {
				border-radius: 50%;
				object-fit: cover;
			}

			.user-pic-default {
				border-radius: 50%;
				position: relative;
				width: 100%;
				height: 100%;

				img {
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);
					width: 100%;
					padding: 25px;
				}
			}
		}

		.action-container {
			display: flex;
			margin-left: auto;
			width: max-content;
			margin-bottom: 10px;
			align-items: center;
			gap: 15px;

			.action-button {
				outline: none;
				border: none;
				width: max-content;
				height: max-content;
				font-size: 14px;
				border-radius: 3px;
				padding: 5px 20px;
				cursor: pointer;
				background: #4e5058;
				color: white;
				transition: 0.3s;

				&.green {
					color: white;
					background: hsl(138.8 calc(1 * 47.1%) 33.3% / 1);
				}

				&.disabled {
					filter: brightness(50%);
					cursor: not-allowed;

					&:hover {
						/* Garde la couleur par défaut */
						&.green {
							background: hsl(138.8 calc(1 * 47.1%) 33.3% / 1);
						}
					}
				}

				&:hover {
					background: #7c7e83;
					&.green {
						background: hsl(137.6 calc(1 * 46.8%) 24.3% / 1);
					}
				}
			}

			.fa-ellipsis-vertical {
				color: #b5bac1;
				font-size: 18px;
				cursor: pointer;
				margin-right: 5px;
				padding: 3px;

				&:hover {
					color: #f3f3f3;
				}
			}
		}
	}

	.body {
		height: 100%;
		background: #18191c;
		display: flex;
		flex-direction: column;
		margin: 15px;
		border-radius: 10px;
		padding: 10px;

		.user-name {
			color: white;
			font-size: 20px;
			margin-bottom: 10px;
		}

		.ant-menu {
			background: transparent;
			border-bottom: 1px solid hsl(216.9 calc(1 * 7.6%) 33.5% / 0.48);
			margin-bottom: 20px;

			.ant-menu-item {
				padding: 0px 0px 10px 0px;
				margin: 0px 20px;
				.ant-menu-title-content {
					font-size: 14px;
					color: #b9bbbe;
				}

				&.ant-menu-item-selected,
				&.ant-menu-item-active {
					.ant-menu-title-content {
						color: white;
					}

					&::after {
						border-bottom: 2px solid white;
					}
				}

				&:first-of-type {
					margin-left: 0px;
				}
				&:last-of-type {
					margin-right: 0px;
				}

				&::after {
					width: 100%;
					left: 0px;
				}
			}
		}

		.user-informations {
			display: flex;
			flex-direction: column;
			height: 100%;

			h3 {
				font-size: 12px;
				text-transform: uppercase;
				color: white;
				font-weight: bold;
			}

			.ant-form {
				width: 100%;
				height: 100%;

				.ant-form-item {
					height: 100%;

					.ant-row {
						height: 100%;

						.ant-col {
							height: 100%;

							.ant-form-item-control-input {
								height: 100%;

								.ant-form-item-control-input-content {
									height: 100%;

									textarea {
										resize: none;
										width: 100%;
										height: 100%;
										border: none;
										outline: none;
										background: transparent;
										color: #cccccc;
										font-size: 14px;

										&::placeholder {
											color: #cccccc;
										}

										&::-webkit-scrollbar {
											width: 4px;
											/* 1 */
										}
										::-webkit-scrollbar-button {
											/* 2 */
											/* background: #202225; */
											display: none;
										}
										::-webkit-scrollbar-track {
											/* 3 */
											background: transparent;
											border-radius: 10px;
										}
										::-webkit-scrollbar-track-piece {
											/* 4 */
											display: none;
										}
										::-webkit-scrollbar-thumb {
											/* 5 */
											background: #2e2f34;
											border-radius: 10px;
										}
										::-webkit-scrollbar-corner {
											/* 6 */
											display: none;
										}
										::-webkit-resizer {
											display: none;
										}
									}
								}
							}
						}
					}
				}
			}

			.created_at {
				color: white;
				font-size: 14px;
				color: #dbdee1;
				margin-bottom: 15px;
			}
		}
	}
`;
