import styled from "styled-components";

export const WindowHeaderWrapper = styled.div`
	display: flex;
	position: absolute;
	top: 0px;
	left: 50%;
	transform: translate(-50%);
	max-width: var(--page-max-width);
	padding: 0px var(--page-gutter);
	width: 100%;
	height: 80px;
	align-items: center;
	z-index: 1;

	.logo {
		color: white;
	}

	.login-link {
		margin-left: auto;
		color: var(--not-quite-black);
		background-color: white;
		border-radius: 40px;
		font-size: 14px;
		padding: 7px 16px;
		cursor: pointer;
		transition: 0.3s;

		&:hover {
			-webkit-box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
			box-shadow: 0 8px 15px rgb(0 0 0 / 20%);
			color: var(--primary-color);
		}
	}
`;
