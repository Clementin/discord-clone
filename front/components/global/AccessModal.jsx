import { Button, Modal } from "antd";
import Link from "next/link";

export default function AccessModal({
	isDiscordCloneWarningOpen,
	setIsDiscordCloneWarningOpen,
}) {
	const handleModalConfirm = () => {
		window.sessionStorage.setItem("discord_clone_warning", true);
		setIsDiscordCloneWarningOpen(false);
	};

	return (
		<Modal
			className="discord_clone_warning"
			title="Information importante avant d'accéder au site !"
			open={isDiscordCloneWarningOpen}
			closable={false}
			centered
			destroyOnClose
			footer={[
				<Button key={"ok"} type="primary" onClick={handleModalConfirm}>
					Accéder au site
				</Button>,
			]}
		>
			<p>
				Ce site n’est pas affilié à{" "}
				<Link
					href="https://discord.com/company?utm_source=dfr"
					legacyBehavior
					passHref
				>
					<a target="_blank" rel="noopener noreferrer">
						Discord Inc
					</a>
				</Link>
				. Les logos, marques et images de{" "}
				<Link href="https://discord.com" legacyBehavior passHref>
					<a target="_blank" rel="noopener noreferrer">
						Discord
					</a>
				</Link>{" "}
				sont la propriété de{" "}
				<Link
					href="https://discord.com/company?utm_source=dfr"
					legacyBehavior
					passHref
				>
					<a target="_blank" rel="noopener noreferrer">
						Discord Inc
					</a>
				</Link>
				.
			</p>

			<p>
				Ce site est un projet clone fictif de{" "}
				<Link href="https://discord.com" legacyBehavior passHref>
					<a target="_blank" rel="noopener noreferrer">
						Discord
					</a>
				</Link>{" "}
				permettant à moi (Clémentin Descamps) de montrer ses compétences en
				développement web.
			</p>

			<p>
				Le site n'est pas responsive, mon but est de montrer mes capacités sur
				certaines fonctionnalités (chat, communication vocal, vidéo temps réel)
				et non mes connaissances CSS
			</p>

			<p>
				Ce projet a été réalisé en NextJS / Laravel et NodeJS pour la partie
				WebSocket
			</p>

			<Link
				href="https://gitlab.com/Clementin/discord-clone"
				legacyBehavior
				passHref
			>
				<a target="_blank" rel="noopener noreferrer">
					https://gitlab.com/Clementin/discord-clone
				</a>
			</Link>
		</Modal>
	);
}
