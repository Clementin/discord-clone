import CrudService from "../../../services/CrudService";
import { emitFriendStatusUpdate } from "../../../socket/handler/FriendHandler";

export function ActionPopoverProfil(
	danger = false,
	disabled = false,
	setter,
	user,
	friendShip
) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"profil " + (disabled ? "disabled" : "")}
				onClick={(e) => {
					setter({ user: user, friendShip: friendShip });
				}}
			>
				Profil
			</button>
		),
	};
}

// Redirige vers la page de discussion
export function ActionPopoverSendMessage(
	danger = false,
	disabled = false,
	userId,
	router,
	setter = false
) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"send " + (disabled ? "disabled" : "")}
				onClick={(e) => {
					router.push("/app/channels/@me/" + userId);
					if (setter) setter(null);
				}}
			>
				Envoyer un message
			</button>
		),
	};
}

export function ActionPopoverCall(danger = false, disabled = false) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"call " + (disabled ? "disabled" : "")}
			>
				Appeler
			</button>
		),
	};
}

export function ActionPopoverVideoCall(danger = false, disabled = false) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"video call " + (disabled ? "disabled" : "")}
			>
				Démarrer un appel vidéo
			</button>
		),
	};
}

export function ActionPopoverVoiceCall(danger = false, disabled = false) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"voice call " + (disabled ? "disabled" : "")}
			>
				Démarrer un appel vocal
			</button>
		),
	};
}

export function ActionPopoverAddNote(
	danger = false,
	disabled = false,
	setter,
	user,
	friendShip
) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"note " + (disabled ? "disabled" : "")}
				onClick={(e) => {
					setter({ user: user, friendShip: friendShip });
				}}
			>
				Ajouter une note
			</button>
		),
	};
}

export function ActionPopoverUnblock(
	danger = false,
	disabled = false,
	friendShip,
	setter = false,
	mutate = false
) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"unblock " + (disabled ? "disabled" : "")}
				onClick={() => {
					if (setter) setter(null);

					CrudService.delete("friend_ship", friendShip.id).then((response) => {
						if (mutate) mutate();
					});
				}}
			>
				Débloquer
			</button>
		),
	};
}

// Copy l'identifiant de l'utilisateur (son nickname)
export function ActionPopoverCopy(danger = false, disabled = false, nickname) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"copy " + (disabled ? "disabled" : "")}
				onClick={(e) => {
					navigator.permissions
						.query({ name: "clipboard-write" })
						.then((result) => {
							if (result.state == "granted" || result.state == "prompt") {
								navigator.clipboard.writeText(nickname);
							}
						});
				}}
			>
				Copier l'identifiant
			</button>
		),
	};
}

export function ActionPopoverNickname(danger = false, disabled = false) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"nickname " + (disabled ? "disabled" : "")}
			>
				Ajouter un pseudo d'ami
			</button>
		),
	};
}

// Supprime une amitié
export function ActionPopoverRemove(
	danger = false,
	disabled = false,
	friendShip,
	onClickFunction = null,
	setter = null,
	mutate = false
) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"remove " + (disabled ? "disabled" : "")}
				onClick={(e) => {
					// Si j'ai une fonction custom

					if (onClickFunction) onClickFunction();
					else {
						if (setter) setter(null);

						CrudService.delete("friend_ship", friendShip.id).then(
							(response) => {
								if (mutate) {
									mutate();
									// Envoie de la notification a l'utilisateur
									emitFriendStatusUpdate(friendShip.otherUser.id);
								}
							}
						);
					}
				}}
			>
				Retirer l'ami
			</button>
		),
	};
}

export function ActionPopoverBlock(danger = false, disabled = false) {
	return {
		type: "button",
		element: (
			<button
				className={(danger ? "danger " : "") + (disabled ? "disabled " : "")}
				key={"block " + (disabled ? "disabled" : "")}
			>
				Blocker
			</button>
		),
	};
}

export function ActionPopoverDivider() {
	return { type: "divider" };
}
