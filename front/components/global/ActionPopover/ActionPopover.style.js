import styled from "styled-components";

export const ActionPopoverWrapper = styled.div`
	position: fixed;
	background: hsl(220 calc(1 * 8.1%) 7.3% / 1);
	padding: 6px 8px;
	border-radius: 4px;
	z-index: 9999;
	box-shadow: 0 8px 16px hsl(0 calc(1 * 0%) 0% / 0.24);
	display: flex;
	flex-direction: column;

	min-width: 188px;
	width: max-content;
	height: max-content;

	button {
		border: none;
		outline: none;
		color: #c7c9ce;
		background: transparent;
		cursor: pointer;
		text-align: left;
		border-radius: 2px;
		margin: 2px 0px;
		min-height: 32px;
		height: 32px;
		box-sizing: border-box;
		padding: 6px 8px;
		font-weight: 400;

		&.danger {
			color: #da373c;
		}

		&.disabled {
			color: #6f6d6d;
			cursor: default;

			&:hover {
				color: #6f6d6d;
				background: transparent;
			}
		}

		&:hover {
			color: white;
			background: #4752c4;

			&.danger {
				background: #da373c;
			}
		}
	}

	.action-popover-divider {
		height: 1px;
		margin: 4px;
		background: #2e2f34;
	}
`;
