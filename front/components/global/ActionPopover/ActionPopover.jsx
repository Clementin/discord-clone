import { useEffect, useState } from "react";
import { ActionPopoverWrapper } from "./ActionPopover.style";

export default function ActionPopover({ content, setContent }) {
	useEffect(() => {
		// Si mon popover vient de s'ouvrir ( utilisé pour détecter l'évenement onBlur )
		if (content) document.getElementById("action-popover").focus();
	}, [content]);

	// Ferme le Popover
	const handleCloseActionPopover = () => {
		setContent(null);
	};

	return content ? (
		<ActionPopoverWrapper
			tabIndex={-1} // Rend le composant focusable mais en dehors de la touche tabulation
			id="action-popover"
			style={{ left: content.positionX, top: content.positionY }} // Positionne le composant a l'endroit ou l'utilisateur à cliqué
			onBlur={(e) => {
				// Si l'élement cliqué n'est pas un enfant de mon popover
				if (!e.currentTarget.contains(e.relatedTarget)) {
					handleCloseActionPopover(e);
				} else {
					// Si j'ai cliqué sur un boutton
					if (
						e.relatedTarget.className != "action-popover-divider" &&
						e.relatedTarget.className != "disabled"
					) {
						// Lance exécute la fonction du boutons avant de fermer le popover
						setTimeout(() => {
							handleCloseActionPopover();
						}, 100);
					}
				}
			}}
		>
			{content.actions.map((action, i) => {
				return action.type == "button" ? (
					// Si je suis un boutton je retourne mon boutton
					action.element
				) : action.type == "divider" ? (
					// Si je suis un divider je retourne mon divider
					<div className="action-popover-divider" key={i}></div>
				) : (
					<></>
				);
			})}
		</ActionPopoverWrapper>
	) : (
		<></>
	);
}
