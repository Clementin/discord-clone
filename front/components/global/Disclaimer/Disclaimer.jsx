import Link from "next/link";
import { DisclaimerWrapper } from "./Disclaimer.style";

export default function Disclaimer() {
	return (
		<DisclaimerWrapper>
			<h2>This is not real discord website</h2>
			<p>
				Go to real{" "}
				<a href="https://discord.com" target="_blank" rel="noopener noreferrer">
					Discord
				</a>{" "}
				website.
			</p>
		</DisclaimerWrapper>
	);
}
