import styled from "styled-components";

export const DisclaimerWrapper = styled.div`
	position: fixed;
	bottom: 20px;
	right: 20px;
	background: transparent;
	z-index: 1;

	h2,
	p {
		margin-bottom: 0px;
		color: #b7b7b7;
		line-height: 1;
		opacity: 0.7;
	}
	h2 {
		margin-bottom: 5px;
	}

	a {
		color: #b7b7b7;
		text-decoration: underline;
	}
`;
