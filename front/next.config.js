const withLess = require("next-with-less");

module.exports = withLess({
	pageExtensions: ["page.jsx"],
	compiler: {
		styledComponents: true,
	},
});
