import { Select } from "antd";

export const requiredRules = {
  required: true,
  message: "Le champ est requis",
};

// Permet de redimensioner le textarea en fonction du texte
export function textarea_auto_grow(e) {
  let textarea = e.target;
  textarea.style.height = "25px";
  textarea.style.height = textarea.scrollHeight + "px";
}

export function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

export const selectFilterOptions = (input, option) => {
  return option.label
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase()
    .includes(
      input
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .toLowerCase()
    );
};

export const SelectOptions = (itemLists, valueField, labelField) => {
  return itemLists?.map((item) => {
    return {
      value: item[valueField],
      label: item[labelField],
    };
  });
};
