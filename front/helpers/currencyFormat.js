export const formatCurrency = (number, maximumFractionDigits) => {
  return new Intl.NumberFormat("fr-FR", {
    style: "currency",
    currency: "EUR",
    maximumFractionDigits,
  }).format(number);
};
