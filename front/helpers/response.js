import { notification } from "antd";

export const notifyResponse = (status, message) => {
  notification.open({
    message: status ? "Succès" : "Erreur",
    description:
      message ??
      (status
        ? "Enregistrement effectué avec succès."
        : "Une erreur est survenue lors de l'enregistrement."),
    className: `notification notification-custom notification-${
      status ? "success" : "error"
    }  notification-in-app`,
    maxCount: 2,
    duration: 10,
  });
};
