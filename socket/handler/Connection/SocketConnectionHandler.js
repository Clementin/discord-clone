const { redisClient } = require("../../redis/redis");

module.exports = (io, socket) => {
	// Je recois les informations de connection de l'utilisateur au socket
	socket.on("connection:join", async ({ token }) => {
		// Identifie l'utilisateur a partir de son Token Sanctum
		fetch(process.env.BACK_URL + "/auth/me", {
			method: "POST",
			headers: {
				Authorization: "Bearer " + token,
				Accept: "application/json",
				"Content-type": "application/json",
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Request-Method": "GET, POST, PUT, OPTIONS",
			},
		}).then(async (response) => {
			let user = await response.json();

			// L'utilisateur à donné le bon token
			if (user.id) {
				// Enregistrement de l'utilisateur dans le redis
				await redisClient.set("token/" + token, JSON.stringify(user));
				// Sauvegarde le token de l'utilisateur qui utilise le socket actuel
				await redisClient.set(
					"socket/" + socket.id + "/token",
					JSON.stringify(token)
				);
				// Regarde si l'utilisateur est connecté depuis d'autre onglet
				let sockets = JSON.parse(
					await redisClient.get("user/" + user.id + "/sockets")
				);

				// Ajoute ou enregistre le socket a l'utilisateur
				await redisClient.set(
					"user/" + user.id + "/sockets",
					JSON.stringify(sockets ? [...sockets, socket.id] : [socket.id])
				);

				// La connexion c'est bien passé
				socket.emit("connection:join:response", true);
			}
			// Le token n'est pas bon
			else {
				socket.emit("connection:join:response", false);
			}
		});
	});

	// Un utilisateur vient de se deconnecter du serveur webSocket
	socket.on("disconnect", async () => {
		// Je trouve le token de l'utilisateur a partir du socket.id
		let token = JSON.parse(
			await redisClient.get("socket/" + socket.id + "/token")
		);

		// Suppressions du token dans le redis
		await redisClient.del("socket/" + token + "/token");

		// Récupere l'utilisateur
		let user = JSON.parse(await redisClient.get("token/" + token));
		// Récupere les sockets de l'utilisateur
		let sockets = JSON.parse(
			await redisClient.get("user/" + user.id + "/sockets")
		);

		// Plusieurs périphérique connecté => suppresions uniquement de celui déconnecté
		if (sockets.length) {
			let newSockets = sockets.filter((item) => item != socket.id);
			await redisClient.set(
				"user/" + user.id + "/sockets",
				JSON.stringify(newSockets)
			);
		}
		// Si un seul périphérique connecté => suppressions complete
		else {
			await redisClient.del("user/" + user.id + "/sockets");
		}

		// Affichage message d'information en mode dev
		if (process.env.ENV == "dev")
			console.log("a user diconnected to websocket");
	});
};
