const { AuthCheck } = require("../../middlewares/AuthMiddleware");
const { redisClient } = require("../../redis/redis");

module.exports = (io, socket) => {
	// Le status d'une amitié vient d'etre modifié
	socket.on(
		"friend:friendShipStatusUpdate",
		async ({ token, addressee_id }) => {
			let user = await AuthCheck(token);

			// Récupére les sockets de l'utilisateur a qui a était adréssé la demande
			let sockets = JSON.parse(
				await redisClient.get("user/" + addressee_id + "/sockets")
			);

			// Si l'utilisateur a des sessions connecté, sinon inutile de continuer
			if (sockets && sockets.length) {
				// Envoie de l'événement a tous les sockets de l'utilisateur

				for (let connectedSocket of sockets) {
					// envoie de l'événement au socket client
					io.to(connectedSocket).emit("friend:friendShipStatusUpdated");
				}
			}
		}
	);
};
