# Route Redis

## Token

- token/{sanctum_token} => Les informations de l'utilisateur associé au token ( permet de vérifier l'authenticité de la connexion )

## Sockets

- socket/{socket_id}/token => Le token de l'utilisateur qui utilise le socket ( utile uniquement pour la déconnexion )

## Utilisateur

- user/{user_id}/sockets => Les sockets de l'utilisateur passé en parametre
