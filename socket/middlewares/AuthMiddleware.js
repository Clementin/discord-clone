const { redisClient } = require("../redis/redis");

function AuthCheck(token) {
	return new Promise(async (resolve, reject) => {
		let user = JSON.parse(await redisClient.get("token/" + token));

		if (user.id) {
			// Retourne mon utilisateur
			resolve(user);
		} else {
			// Déconnexion de l'utilisateur
			socket.emit("connection:join:response", false);
		}
	});
}

module.exports = {
	AuthCheck,
};
