require("dotenv").config();
const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const SocketConnectionHandler = require("./handler/Connection/SocketConnectionHandler");
const SocketFriendHandler = require("./handler/Friend/SocketFriendHandler");
const { redisClient } = require("./redis/redis");
const io = new Server(server, {
	pingInterval: 5000,
	pingTimeout: 5000,
	cors: { origin: "http://localhost:3000" },
});

// Route web de test
app.get("/", (req, res) => {
	res.send("<h1>socket works</h1>");
});

/**
 * Un utilisateur vient de se connecter au websocket
 */
io.on("connection", (socket) => {
	if (process.env.ENV == "dev") console.log("a user connected to websocket");

	// Handlers
	SocketConnectionHandler(io, socket);
	SocketFriendHandler(io, socket);
});

// Démarage du serveur nodeJS
if (process.env.PORT) {
	server.listen(process.env.PORT, async () => {
		console.log("listening on port " + process.env.PORT);

		// Connexion a redis
		await redisClient.connect();
		redisClient.flushDb();
		console.log("redis flushed");

		// Une érreur avec le serveur redis est survenue
		redisClient.on("error", (err) => console.log("Redis Client Error", err));
	});
} else {
	console.log("Aucun port spécifié. Vérifier fichier environnement");
}
