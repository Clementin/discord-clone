# Clone Discord

Listes des port utilisés pour l'application

- Front : 3000
- Back : 8000
- WebSocket : 8001
- Redis : 6379

## Installation

### Installation de Redis

> sudo snap install redis

> redis-server --port 9001

## Lancement du projet en mode dev

> cd api && composer install && php artisan serve

> cd api && php artisan migrate:fresh --seed

> cd front && yarn && yarn dev

> cd socket && npm i && npm run dev
