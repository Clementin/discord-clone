## Notes

Web routing is disabled by default in web.php. There is a fallback on .env variable APP_URL

## TODO

Generate new encryption key for Laravel API

```bash
php artisan key:generate
```

The command below will print the path to the php.ini file that your server is using.

php -i | grep php.ini
Next.

sudo nano /etc/php/7.4/cli/php.ini
The values of post_max_size, upload_max_filesize and memory_limit by default have the value of 8M, 2M, and 128M respectively.

Search for those variables and change their values, whilst ensuring that the sizes follow the same ratio as the default values.

See example below:

post_max_size = 2G
upload_max_filesize = 1G
memory_limit = 3G
For "heavy-loaded sites", it is useful to have FPM installed and running.

sudo apt install php7.4-fpm -y
sudo service php7.4-fpm start
Finally, restart your web server.

sudo service apache2 restart
