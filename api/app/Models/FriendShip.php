<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Auth;

class FriendShip extends Pivot
{
    protected $fillable = [
        "requester_id",
        "addressee_id",
        'status'
    ];

    protected $appends = [
        "otherUser"
    ];

    public function requester()
    {
        return $this->belongsTo(User::class, "requester_id");
    }

    public function addresee()
    {
        return $this->belongsTo(User::class, "addressee_id");
    }

    // Retourne l'utilisateur dans la relation qui n'est pas l'utilisateur connecté
    public function getOtherUserAttribute()
    {
        return $this->requester_id == Auth::user()->id ? $this->addresee :  $this->requester;
    }
}
