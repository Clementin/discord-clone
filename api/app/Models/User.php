<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, HasUlids;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'nickname',
        'password',
        'color',
        "picture"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function servers()
    {
        return $this->belongsToMany(Server::class);
    }

    public function ownedServers()
    {
        return $this->hasMany(Server::class);
    }

    // Les notes que l'utilisateur a écrit sur les autres utilisateur
    public function noteAsWriter()
    {
        return $this->hasMany(Note::class, "writer_id");
    }
}
