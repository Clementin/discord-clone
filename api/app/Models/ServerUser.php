<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ServerUser extends Pivot
{
    protected $fillable = ["server_id", "user_id"];
}
