<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory, HasUlids;

    protected $fillable = [
        'name',
        "short_name",
        'picture',
        'owner_id'
    ];

    public function users()
    {
        return $this->belongsToMany(Server::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
