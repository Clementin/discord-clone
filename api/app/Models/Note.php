<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $fillable = [
        "writer_id",
        "target_id",
        "text"
    ];

    public function writer()
    {
        return $this->belongsTo(User::class, "writer_id");
    }

    public function target()
    {
        return $this->belongsTo(User::class, "target_id");
    }
}
