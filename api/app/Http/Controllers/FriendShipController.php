<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFriendShipRequest;
use App\Models\FriendShip;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FriendShipController extends Controller
{
    public function index()
    {
        // Retourne toutes les relations de l'utilisateur connecté
        $friendShips = FriendShip::where('requester_id', Auth::user()->id)
            ->orWhere('addressee_id', Auth::user()->id)
            ->get();

        return response()->json($friendShips);
    }

    public function store(StoreFriendShipRequest $request)
    {
        // trouver la personne à ajouter en Amis
        $addresseUser = User::where('nickname', $request->nickname)->first();

        // L'utilisateur n'existe pas et n'est pas l'utilisateur qui fais la demande
        if (!$addresseUser || Auth::user()->id == $addresseUser->id) {
            return response()->json(['status' => 0, "message" => "Mhm, ça n'a pas marché. Vérifie bien que la casse, l'orthographe, les espaces et les chiffres sont corrects."]);
        }

        // Je récupere toutes les amitiés de l'utilisateur
        foreach (FriendShip::where('requester_id', Auth::user()->id)->orWhere('addressee_id', Auth::user()->id)->get() as $friendShip) {
            // Si une amitié contient l'utilisateur de la nouvelle demande alors érreur
            if ($friendShip->requester_id == $addresseUser->id || $friendShip->addressee_id == $addresseUser->id) {
                return response()->json(['status' => 0, "message" => "Tu es déjà ami(e) avec cet utilisateur ou alors il t'a bloqué !"]);
            }
        }

        // Enregistrement de la demande d'amitié
        $friendShip = FriendShip::create([
            'requester_id' => Auth::user()->id,
            "addressee_id" => $addresseUser->id,
            "status" => "pending"
        ]);

        return response()->json(['status' => 1, "message" => "Bravo ! Ta demande d'ami a été envoyée à " . $addresseUser->nickname, "friendShip" => $friendShip]);
    }

    public function destroy(FriendShip $friendShip)
    {
        $friendShip->delete();

        return response(['status' => 1]);
    }

    public function update(FriendShip $friendShip)
    {
        $friendShip->update([
            "status" => "accepted"
        ]);

        return response()->json(['status' => 1, "friendShip" => $friendShip]);
    }
}
