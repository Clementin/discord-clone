<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    // User connect to webApp    
    public function connect(AuthRequest $request)
    {
        $errorResponse = response()->json(['status' => 0, "errors" => ['email' => ["Identifiant ou mot de passe invalide."], "password" => ['Identifiant ou mot de passe invalide.']]]);

        // Get user from email
        $user = User::where('email', $request->email)->first();

        // user exist
        if ($user) {
            // Check password
            if (Hash::check($request->password, $user->password)) {
                // Token creation
                $token = $user->createToken('auth_token')->plainTextToken;

                return response()->json(['status' => 1, "message" => "Ok", "token" => $token, "user" => $user]);
            } else {
                return $errorResponse;
            }
        } else {
            return $errorResponse;
        }
    }

    // Return authenticated User 
    public function me()
    {
        return Auth::user();
    }
}
