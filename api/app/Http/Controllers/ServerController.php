<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreServerRequest;
use App\Http\Requests\UpdateServerRequest;
use App\Models\Server;
use App\Models\ServerUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Auth::user()->servers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServerRequest $request)
    {
        $response = DB::transaction(function () use ($request) {
            // Create short_name
            $count = 0;
            $short_name = "";
            foreach (explode(' ', $request->name) as $word) {
                if ($count < 5) {
                    $short_name = $short_name . $word[0];
                    $count++;
                }
            }

            $server = Server::create(["name" => $request->name, "short_name" => $short_name, "owner_id" => Auth::user()->id]);

            // Ajout de la photo du serveur
            if ($request->picture) {
                // Detect file extensions
                $ext = explode(';base64', $request->picture);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                if ($ext == "png" || $ext == "jpeg" || $ext == "jpg" || $ext == "svg+xml") {
                    $image = $request->picture;  // your base64 encoded
                    $image = str_replace("data:image/" . $ext . ";base64,", '', $image);
                    $image = str_replace(' ', '+', $image);

                    if ($ext == "svg+xml") {
                        $ext = "svg";
                    }
                    $picturePath =  '/images/server_picture/' . $server->id . "." . $ext;

                    // Save picture on disk
                    Storage::disk('local')->put($picturePath, base64_decode($image));
                    // Update link
                    $server->update(['picture' => $picturePath]);


                    // Add owner as first user
                    ServerUser::create(['server_id' => $server->id, "user_id" => Auth::user()->id]);
                } else {
                    DB::rollBack();
                    return ["status" => 0, "message" => "Format de photo non supporté", "type" => $ext];
                }
            }

            return ["status" => 1, "message" => "Serveur crée", "server" => $server];
        });


        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function show(Server $server)
    {
        // Check if user have access to server
        $server_user = ServerUser::where('server_id', $server->id)->where("user_id", Auth()->user()->id)->get();
        if (count($server_user)) {
            return response()->json(["status" => 1, "server" => $server]);
        } else {
            return response()->json(["status" => 0, "message" => "access forbiden to this server"]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServerRequest  $request
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServerRequest $request, Server $server)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function destroy(Server $server)
    {
        //
    }
}
