<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFriendShipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "nickname" => "required"
        ];
    }

    public function messages()
    {
        return [
            "*.required" => "Mhm, ça n'a pas marché. Vérifie bien que la casse, l'orthographe, les espaces et les chiffres sont corrects."
        ];
    }
}
