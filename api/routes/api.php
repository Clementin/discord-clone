<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FriendShipController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\ServerController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::post('', 'connect');
    Route::post('me', "me")->middleware('auth:sanctum');
});


// Protected route 
Route::middleware('auth:sanctum')->group(function () {
    Route::apiResources([
        "servers" => ServerController::class,
        "friend_ship" => FriendShipController::class,
        "notes" => NoteController::class,
    ]);


    // Note
    Route::controller(NoteController::class)->prefix('note')->group(function () {
        Route::get('/{targetUser}', "getNote");
    });
});
