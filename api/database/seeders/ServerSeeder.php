<?php

namespace Database\Seeders;

use App\Models\Server;
use App\Models\ServerUser;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Server::create(
            ["name" => "LES BANLIEUSARDS DAWIN", "short_name" => "LBD", "owner_id" => User::all()->first()->id, 'picture' => "/images/server_picture/lbd_default.jpg"]
        );

        foreach (Server::all() as $server) {
            ServerUser::create(['server_id' => $server->id, "user_id" => User::all()->first()->id]);
        }
    }
}
