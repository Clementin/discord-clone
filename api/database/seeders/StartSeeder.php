<?php

namespace Database\Seeders;

use App\Models\FriendShip;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crée le compte Clémentin
        $devAccount = User::create([
            "email" => env("ADMIN_EMAIL"),
            "nickname" => env("ADMIN_NICKNAME"),
            "password" => Hash::make(env("ADMIN_PASSWORD")),
            "color" => "#ec4444"
        ]);

        // Crée des comptes utilisateurs random pour l"amitié
        User::factory()->count(10)->create();

        // tableau des status
        $status = [
            "accepted",
            "pending",
            "blocked"
        ];

        // Ajoute des amitiés au comptes Clémentin
        foreach (User::where("id", "!=", $devAccount->id)->get() as $user) {
            // Si l'utilisateur n'a aucune amitié
            if (FriendShip::where('requester_id', "!=", $user->id)->where('addressee_id', "!=", $user->id)->get()) {
                FriendShip::create(["requester_id" => $devAccount->id, "addressee_id" => $user->id, "status" => $status[rand(0, count($status) - 1)]]);
            }
        };

        // Crée 10 comptes utilisateurs random
        User::factory()->count(10)->create();
    }
}
